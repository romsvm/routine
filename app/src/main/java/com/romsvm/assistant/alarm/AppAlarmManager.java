package com.romsvm.assistant.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksRepository;

/**
 * Created by Roman on 12.04.2017.
 */

public class AppAlarmManager {

    public static final String ALARM_ACTION = "appAlarmManager.ALARM_ACTION";
    private static AppAlarmManager INSTANCE;

    public static AppAlarmManager getInstance(Context context,
                                              TasksRepository tasksRepository) {
        if (INSTANCE == null) {
            INSTANCE = new AppAlarmManager(context, tasksRepository);
        }

        return INSTANCE;
    }

    private final AlarmManager alarmManager;
    private final TasksRepository tasksRepository;
    private final Context context;

    public AppAlarmManager(Context context, TasksRepository tasksRepository) {
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        this.tasksRepository = tasksRepository;
        this.context = context;
    }

    public void addAlarm(Task task) {
        Intent intent = new Intent(context, AppBroadcastReceiver.class);
        intent.setAction(AppAlarmManager.ALARM_ACTION);
        intent.putExtra(AppBroadcastReceiver.TASK_NAME, task.getTitle());
        intent.putExtra(AppBroadcastReceiver.PROJECT_NAME,
                tasksRepository.getProjectWithId(task.getProjectId()).getTitle());
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, task.getReminder(), alarmIntent);
    }
}
