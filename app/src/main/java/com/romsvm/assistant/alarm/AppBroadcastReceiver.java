package com.romsvm.assistant.alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.romsvm.assistant.R;
import com.romsvm.assistant.launch.LaunchActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Roman on 12.04.2017.
 */

public class AppBroadcastReceiver extends BroadcastReceiver {

    public static final String PROJECT_NAME = "projectName";
    public static final String TASK_NAME = "taskName";

    @Override
    public void onReceive(Context context, Intent intent) {
        //TODO пересоздать все alarm при перезагрузке устройства
        //TODO переходить на экран SelectedTasks и фильтровать по проекту
        //TODO показать пользователю экран настроек autostart
        if (intent.getAction() != null) {
            if (intent.getAction().equals(AppAlarmManager.ALARM_ACTION)) {
                String taskName = intent.getStringExtra(AppBroadcastReceiver.TASK_NAME);
                String projectName = intent.getStringExtra(AppBroadcastReceiver.PROJECT_NAME);

                Intent resultIntent = new Intent(context, LaunchActivity.class);

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addParentStack(LaunchActivity.class);
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(context)
                                .setAutoCancel(true)
                                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                                .setContentTitle(taskName)
                                .setContentText(projectName)
                                .setContentIntent(resultPendingIntent)
                                .setPriority(Notification.PRIORITY_MAX)
                                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND |
                                             Notification.DEFAULT_VIBRATE);

                int notificationId = 1;
                NotificationManager notificationManager =
                        (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(notificationId, builder.build());
            } else if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

            }
        }
    }
}