package com.romsvm.assistant.taskaddedit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.UseCaseHandler;
import com.romsvm.assistant.data.filter.TasksFilterType;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.taskaddedit.domain.usecase.DeleteTask;
import com.romsvm.assistant.taskaddedit.domain.usecase.GetTask;
import com.romsvm.assistant.taskaddedit.domain.usecase.SaveTask;
import com.romsvm.assistant.taskaddedit.domain.usecase.UpdateTask;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 28.02.2017.
 */

public class TaskAddEditPresenter implements TaskAddEditContract.Presenter {

    private static TaskAddEditPresenter instance;

    public static TaskAddEditPresenter getInstance(@NonNull UseCaseHandler useCaseHandler,
                                                   @Nullable String taskId,
                                                   @Nullable String filterName,
                                                   @Nullable String filterValue,
                                                   @NonNull TaskAddEditContract.View taskAddEditView,
                                                   @NonNull GetTask getTask,
                                                   @NonNull SaveTask saveTask,
                                                   @NonNull UpdateTask updateTask,
                                                   @NonNull DeleteTask deleteTask) {
        if (instance == null) {
            instance = new TaskAddEditPresenter(useCaseHandler, taskId, filterName, filterValue,
                    taskAddEditView, getTask, saveTask, updateTask,
                    deleteTask);
        } else {
            instance.taskId = taskId;
            instance.filterName = filterName;
            instance.filterValue = filterValue;
            instance.taskAddEditView = taskAddEditView;
        }

        instance.taskAddEditView.setPresenter(instance);

        return instance;
    }

    private TaskAddEditContract.View taskAddEditView;
    private final GetTask getTask;
    private final SaveTask saveTask;
    private final UpdateTask updateTask;
    private final DeleteTask deleteTask;
    private final UseCaseHandler useCaseHandler;

    @Nullable
    private String taskId;

    @Nullable
    private String filterName;

    @Nullable
    private String filterValue;

    private Bundle viewState = new Bundle();

    private TaskAddEditPresenter(@NonNull UseCaseHandler useCaseHandler, @Nullable String taskId,
                                 @Nullable String filterName, @Nullable String filterValue,
                                 @NonNull TaskAddEditContract.View taskAddEditView,
                                 @NonNull GetTask getTask, @NonNull SaveTask saveTask,
                                 @NonNull UpdateTask updateTask, @NonNull DeleteTask deleteTask) {
        this.useCaseHandler = checkNotNull(useCaseHandler);
        this.taskId = taskId;
        this.filterName = filterName;
        this.filterValue = filterValue;
        this.taskAddEditView = checkNotNull(taskAddEditView);
        this.getTask = checkNotNull(getTask);
        this.saveTask = checkNotNull(saveTask);
        this.updateTask = checkNotNull(updateTask);
        this.deleteTask = checkNotNull(deleteTask);

        this.taskAddEditView.setPresenter(this);
    }

    @Override
    public void start() {

        Calendar calendarToday = Calendar.getInstance();
        calendarToday.set(Calendar.HOUR_OF_DAY, 0);
        calendarToday.set(Calendar.MINUTE, 0);
        calendarToday.set(Calendar.SECOND, 0);
        calendarToday.set(Calendar.MILLISECOND, 0);

        Calendar calendarTomorrow = Calendar.getInstance();
        calendarTomorrow.set(Calendar.HOUR_OF_DAY, 0);
        calendarTomorrow.set(Calendar.MINUTE, 0);
        calendarTomorrow.set(Calendar.SECOND, 0);
        calendarTomorrow.set(Calendar.MILLISECOND, 0);
        calendarTomorrow
                .set(Calendar.DAY_OF_MONTH, calendarTomorrow.get(Calendar.DAY_OF_MONTH) + 1);

        if (taskId != null && taskId.length() > 0) {
            populateTask();
        } else {
            if (filterValue != null) {
                if (filterName.equals(TasksFilterType.BY_DATE.name()) &&
                    filterValue.equals(String.valueOf(calendarToday.getTimeInMillis()))) {
                    taskAddEditView.setDueDate(calendarToday.getTimeInMillis());
                } else if (filterName.equals(TasksFilterType.BY_DATE.name()) &&
                           filterValue.equals(String.valueOf(calendarTomorrow.getTimeInMillis()))) {
                    taskAddEditView.setDueDate(calendarTomorrow.getTimeInMillis());
                }
            }
        }
    }

    @Override
    public Bundle getViewState() {
        return viewState;
    }

    @Override
    public void saveTask(String title, String comment, boolean isComplete, Task.Priority priority,
                         String dueDate, String reminder) {

        long parsedDueDate = 0;
        long parsedReminder = 0;

        if (dueDate.contains(".")) {
            DateFormat dateFormatDueDate = new SimpleDateFormat("dd.MM.yyyy");
            try {
                Date dateDueDate = dateFormatDueDate.parse(dueDate);
                parsedDueDate = dateDueDate.getTime();

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (reminder.contains(":")) {
                String strDueDateAndReminder = dueDate + ":" + reminder;
                DateFormat dateFormatReminder = new SimpleDateFormat("dd.MM.yyyy:HH:mm");

                try {
                    Date dateDueDateAndReminder = dateFormatReminder.parse(strDueDateAndReminder);
                    parsedReminder = dateDueDateAndReminder.getTime();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Calendar", "dueDate = " + parsedDueDate);
        }

        if (isNewTask()) {
            createTask(title, comment, isComplete, priority, parsedDueDate, parsedReminder);
        } else {
            updateTask(title, comment, isComplete, priority, parsedDueDate, parsedReminder);
        }
    }

    @Override
    public void populateTask() {
        if (taskId == null) {
            throw new RuntimeException("populateTask() was called but task is new");
        }

        useCaseHandler.execute(getTask, new GetTask.RequestValues(taskId),
                new UseCase.UseCaseCallback<GetTask.ResponseValue>() {
                    @Override
                    public void onSuccess(GetTask.ResponseValue response) {
                        showTask(response.getTask());
                    }

                    @Override
                    public void onError() {
                        showEmptyTaskError();
                    }
                });
    }

    @Override
    public void deleteTask() {
        if (taskId == null) {
            throw new RuntimeException("deleteTask was called but task is new");
        }

        useCaseHandler.execute(deleteTask, new DeleteTask.RequestValues(taskId),
                new UseCase.UseCaseCallback<DeleteTask.ResponseValue>() {
                    @Override
                    public void onSuccess(DeleteTask.ResponseValue response) {
                        if (taskAddEditView.isActive()) {
                            taskAddEditView.showTasksList();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    private void showTask(Task task) {
        if (taskAddEditView.isActive()) {
            taskAddEditView.setTitle(task.getTitle());
            taskAddEditView.setComment(task.getComment());
            taskAddEditView.setIsComplete(task.getIsComplete());
            taskAddEditView.setPriority(task.getPriority().name());
            taskAddEditView.setDueDate(task.getDueDate());
            taskAddEditView.setReminder(task.getReminder());
        }
    }

    private void showEmptyTaskError() {
        if (taskAddEditView.isActive()) {
            taskAddEditView.showEmptyTaskError();
        }
    }

    private boolean isNewTask() {
        return taskId == null;
    }

    private void createTask(final String title, final String comment, final boolean isComplete,
                            final Task.Priority priority, final long dueDate, final long reminder) {

        //task become id from UUID
        Task newTask = new Task.Builder(title)
                .setProjectId(
                        filterName.equals(TasksFilterType.BY_PROJECT.name()) ? filterValue : "")
                .setComment(comment)
                .setIsComplete(isComplete)
                .setPriority(priority)
                .setDueDate(dueDate)
                .setReminder(reminder)
                .setDateCreate(new Date().getTime())
                .build();

        if (newTask.isEmpty()) {
            if (taskAddEditView.isActive()) {
                taskAddEditView.showEmptyTaskError();
            }
        } else {
            useCaseHandler.execute(saveTask, new SaveTask.RequestValue(newTask),
                    new UseCase.UseCaseCallback<SaveTask.ResponseValue>() {
                        @Override
                        public void onSuccess(SaveTask.ResponseValue response) {
                            if (taskAddEditView.isActive()) {
                                taskAddEditView.showTasksList();
                            }
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    private void updateTask(final String title, final String comment, final boolean isComplete,
                            final Task.Priority priority, final long dueDate, final long reminder) {
        if (taskId == null) {
            throw new RuntimeException("updateSelectedTasks() was called but task is new");
        }

        useCaseHandler.execute(getTask, new GetTask.RequestValues(taskId),
                new UseCase.UseCaseCallback<GetTask.ResponseValue>() {
                    @Override
                    public void onSuccess(GetTask.ResponseValue response) {
                        Task updatedTask = response.getTask();

                        //Update everything but taskId and projectId
                        Task newTask = new Task.Builder(title)
                                .setId(updatedTask.getId())
                                .setProjectId(updatedTask.getProjectId())
                                .setComment(comment)
                                .setIsComplete(isComplete)
                                .setPriority(priority)
                                .setDueDate(dueDate)
                                .setReminder(reminder)
                                .setDateCreate(updatedTask.getDateCreate())
                                .setDateUpdate(new Date().getTime())
                                .build();

                        ArrayList<Task> list = new ArrayList<Task>();
                        list.add(newTask);

                        useCaseHandler.execute(updateTask,
                                new UpdateTask.RequestValues(list),
                                new UseCase.UseCaseCallback<UpdateTask.ResponseValue>() {
                                    @Override
                                    public void onSuccess(
                                            UpdateTask.ResponseValue response) {
                                        if (taskAddEditView
                                                .isActive()) {
                                            taskAddEditView
                                                    .showTasksList();
                                        }
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                    }

                    @Override
                    public void onError() {

                    }
                });
    }
}
