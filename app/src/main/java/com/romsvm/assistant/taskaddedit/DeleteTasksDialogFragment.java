package com.romsvm.assistant.taskaddedit;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.romsvm.assistant.R;

/**
 * Created by Roman on 09.03.2017.
 */

public class DeleteTasksDialogFragment extends DialogFragment {

    public interface DeleteTaskDialogListener {
        public void onDeleteTaskDialogPositiveClick();

        public void onDeleteTaskDialogNegativeClick();
    }

    private DeleteTaskDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            FragmentManager supportFragmentManager =
                    ((TaskAddEditActivity) context).getSupportFragmentManager();
            Fragment fragment = supportFragmentManager.findFragmentById(R.id.contentFrame);

            listener = (DeleteTaskDialogListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() + "must implement DeleteTaskDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.activity_projectdetail_delete_selected_task);
        builder.setPositiveButton(R.string.activity_projectdetail_delete_selected_task_delete,
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialogInterface, int i) {
                                          listener.onDeleteTaskDialogPositiveClick();
                                      }
                                  });
        builder.setNegativeButton(R.string.activity_projectdetail_delete_selected_task_cancel,
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialogInterface, int i) {
                                          listener.onDeleteTaskDialogNegativeClick();
                                      }
                                  });

        return builder.create();
    }
}
