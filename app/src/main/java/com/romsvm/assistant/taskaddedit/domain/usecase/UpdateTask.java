package com.romsvm.assistant.taskaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.alarm.AppAlarmManager;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksRepository;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 01.03.2017.
 */

public class UpdateTask extends UseCase<UpdateTask.RequestValues, UpdateTask.ResponseValue> {

    private final AppAlarmManager appAlarmManager;
    private final TasksRepository tasksRepository;

    public UpdateTask(@NonNull TasksRepository tasksRepository,
                      @NonNull AppAlarmManager appAlarmManager) {
        this.appAlarmManager = checkNotNull(appAlarmManager);
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    @Override
    protected void executeUseCase(final UpdateTask.RequestValues values) {
        tasksRepository.updateSelectedTasks(values.getTaskList());
        getUseCaseCallback().onSuccess(new UpdateTask.ResponseValue(values.getTaskList()));

        Task task = values.getTaskList().get(0);

        if (task != null && task.getReminder() > 0) {
            appAlarmManager.addAlarm(task);
        }

        Log.d(UseCase.TAG, "UpdateTask");
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final List<Task> taskList;

        public RequestValues(@NonNull List<Task> taskList) {
            this.taskList = checkNotNull(taskList, "taskList cannot be null!");
        }

        public List<Task> getTaskList() {
            return taskList;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final List<Task> taskList;

        public ResponseValue(@NonNull List<Task> taskList) {
            this.taskList = checkNotNull(taskList, "taskList cannot be null!");
        }

        public List<Task> getTaskList() {
            return taskList;
        }
    }
}
