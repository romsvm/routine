package com.romsvm.assistant.taskaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.source.TasksRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 07.03.2017.
 */

public class DeleteTask extends UseCase<DeleteTask.RequestValues, DeleteTask.ResponseValue> {
    public DeleteTask(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    private final TasksRepository tasksRepository;

    @Override
    protected void executeUseCase(final DeleteTask.RequestValues values) {
        tasksRepository.deleteTask(values.getTaskId());
        getUseCaseCallback().onSuccess(new DeleteTask.ResponseValue());
        Log.d(UseCase.TAG, "DeleteTask");
    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final String taskId;

        public RequestValues(@NonNull String taskId) {
            this.taskId = checkNotNull(taskId, "taskList cannot be null!");
        }

        public String getTaskId() {
            return taskId;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {
    }
}
