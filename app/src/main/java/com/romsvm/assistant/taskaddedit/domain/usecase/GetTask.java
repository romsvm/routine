package com.romsvm.assistant.taskaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 01.03.2017.
 */

public class GetTask extends UseCase<GetTask.RequestValues, GetTask.ResponseValue> {

    public GetTask(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    private final TasksRepository tasksRepository;

    @Override
    protected void executeUseCase(final GetTask.RequestValues values) {
        tasksRepository.getTask(values.getTaskId(), new TasksDataSource.GetTaskCallback() {
            @Override
            public void onTaskLoaded(Task task) {
                if (task != null) {
                    GetTask.ResponseValue responseValue = new GetTask.ResponseValue(task);
                    getUseCaseCallback().onSuccess(responseValue);
                } else {
                    getUseCaseCallback().onError();
                }
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });

        Log.d(UseCase.TAG, "GetTask");
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final String taskId;

        public RequestValues(@NonNull String taskId) {
            this.taskId = checkNotNull(taskId, "taskId cannot be null!");
        }

        public String getTaskId() {
            return taskId;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private Task task;

        public ResponseValue(@NonNull Task task) {
            this.task = checkNotNull(task, "task cannot be null!");
        }

        public Task getTask() {
            return task;
        }
    }
}