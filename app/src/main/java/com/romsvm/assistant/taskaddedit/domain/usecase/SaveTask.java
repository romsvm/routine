package com.romsvm.assistant.taskaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.alarm.AppAlarmManager;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 01.03.2017.
 */

public class SaveTask extends UseCase<SaveTask.RequestValue, SaveTask.ResponseValue> {
    private final AppAlarmManager appAlarmManager;
    private final TasksRepository tasksRepository;

    public SaveTask(@NonNull TasksRepository tasksRepository,
                    @NonNull AppAlarmManager appAlarmManager) {
        this.appAlarmManager = checkNotNull(appAlarmManager);
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    @Override
    protected void executeUseCase(final RequestValue values) {
        Task task = values.getTask();
        this.tasksRepository.saveTask(task);

        getUseCaseCallback().onSuccess(new SaveTask.ResponseValue(task));

        if (task.getReminder() > 0) {
            appAlarmManager.addAlarm(task);
        }

        Log.d(UseCase.TAG, "SaveTask");
    }

    public static final class RequestValue implements UseCase.RequestValues {
        private final Task task;

        public RequestValue(@NonNull Task task) {
            this.task = checkNotNull(task);
        }

        public Task getTask() {
            return task;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {
        private final Task task;

        public ResponseValue(@NonNull Task task) {
            this.task = checkNotNull(task);
        }

        public Task getTask() {
            return task;
        }
    }
}
