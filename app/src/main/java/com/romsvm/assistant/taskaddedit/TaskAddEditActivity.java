package com.romsvm.assistant.taskaddedit;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.romsvm.assistant.Injection;
import com.romsvm.assistant.R;
import com.romsvm.assistant.util.ActivityUtils;
import com.romsvm.assistant.util.EspressoIdlingResource;

/**
 * Created by Roman on 28.02.2017.
 */

public class TaskAddEditActivity extends AppCompatActivity {
    public static final String EXTRA_TASK_ID = "TASK_ID";
    public static final String EXTRA_FILTER_NAME = "CATEGORY";
    public static final String EXTRA_FILTER_VALUE = "CATEGORY_VALUE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_add_edit);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        TaskAddEditFragment taskAddEditFragment = (TaskAddEditFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        String taskId = getIntent().getStringExtra(TaskAddEditActivity.EXTRA_TASK_ID);
        String filterName = getIntent().getStringExtra(TaskAddEditActivity.EXTRA_FILTER_NAME);
        String filterValue = getIntent().getStringExtra(TaskAddEditActivity.EXTRA_FILTER_VALUE);

        if (taskAddEditFragment == null) {
            taskAddEditFragment = TaskAddEditFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), taskAddEditFragment,
                                                R.id.contentFrame);
        }

        if (taskId != null) {
            this.setTitle(R.string.title_activity_task_edit);
        } else {
            this.setTitle(R.string.title_activity_task_add);
        }

        TaskAddEditPresenter
                .getInstance(Injection.provideUseCaseHandler(), taskId, filterName, filterValue,
                             taskAddEditFragment, Injection.provideGetTask(getApplicationContext()),
                             Injection.provideSaveTask(getApplicationContext()),
                             Injection.provideUpdateTask(getApplicationContext()),
                             Injection.provideDeleteTask(getApplicationContext()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
