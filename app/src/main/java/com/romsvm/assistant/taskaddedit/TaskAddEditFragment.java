package com.romsvm.assistant.taskaddedit;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.romsvm.assistant.R;
import com.romsvm.assistant.data.model.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 28.02.2017.
 */

public class TaskAddEditFragment extends Fragment
        implements TaskAddEditContract.View, DeleteTasksDialogFragment.DeleteTaskDialogListener {

    public static TaskAddEditFragment newInstance() {
        return new TaskAddEditFragment();
    }

    public TaskAddEditFragment() {

    }

    TextView tvTitle;
    TextView tvComment;
    CheckBox cbIsComplete;
    Spinner spPriority;
    TextView tvDueDate;
    TextView tvReminder;

    ArrayAdapter<Task.Priority> arrayAdapter;
    TaskAddEditContract.Presenter presenter;

    AlarmManager alarmManager;

    private static final String STATE_TITLE = "stateTitle";
    private static final String STATE_COMMENT = "stateComment";
    private static final String STATE_COMPLETE = "stateComplete";
    private static final String STATE_PRIORITY = "statePriority";
    private static final String STATE_DUE_DATE = "stateDueDate";
    private static final String STATE_REMINDER = "stateReminder";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);

        setHasOptionsMenu(true);

        alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);

        View root = inflater.inflate(R.layout.fragment_task_add_edit, container, false);
        tvTitle = (TextView) root.findViewById(R.id.et_add_task_title);
        tvComment = (TextView) root.findViewById(R.id.et_add_task_comment);
        cbIsComplete = (CheckBox) root.findViewById(R.id.cb_iscomplete);
        spPriority = (Spinner) root.findViewById(R.id.sp_priority);

        arrayAdapter =
                new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_dropdown_item,
                                   Task.Priority.values());
        spPriority.setAdapter(arrayAdapter);
        spPriority.setSelection(Task.Priority.values().length - 1);

        final Calendar calendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener onDateSetListener =
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        calendar.set(i, i1, i2);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.US);

                        tvDueDate.setText(dateFormat.format(calendar.getTime()));
                    }
                };

        tvDueDate = (TextView) root.findViewById(R.id.tv_duedate);
        tvDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(TaskAddEditFragment.this.getContext(), onDateSetListener,
                                     calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                     calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tvDueDate.setText(R.string.activity_taskaddedit_hint_duedate);

        final TimePickerDialog.OnTimeSetListener onTimeSetListener =
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        tvReminder.setText(i + ":" + (i1 > 0 ? i1 : "00"));

                        Intent intent = new Intent(getContext(), TaskAddEditActivity.class);
                        PendingIntent alarmIntent =
                                PendingIntent.getBroadcast(getContext(), 0, intent, 0);
                        alarmManager.set(AlarmManager.RTC_WAKEUP,
                                         SystemClock.elapsedRealtime() + 10 * 1000, alarmIntent);
                    }
                };

        tvReminder = (TextView) root.findViewById(R.id.tv_reminder);
        tvReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(TaskAddEditFragment.this.getContext(), onTimeSetListener, 0, 0,
                                     true).show();
            }
        });
        tvReminder.setText(R.string.activity_taskaddedit_hint_reminder);

        if (!presenter.getViewState().isEmpty()) {
            Bundle viewState = presenter.getViewState();

            tvTitle.setText(viewState.getString(STATE_TITLE));
            tvComment.setText(viewState.getString(STATE_COMMENT));
            spPriority.setSelection(arrayAdapter.getPosition(
                    Enum.valueOf(Task.Priority.class, viewState.getString(STATE_PRIORITY))));
            cbIsComplete.setChecked(viewState.getBoolean(STATE_COMPLETE));
            tvDueDate.setText(viewState.getString(STATE_DUE_DATE));
            tvReminder.setText(viewState.getString(STATE_REMINDER));

            viewState.clear();
        }

        getActivity().invalidateOptionsMenu();

        FloatingActionButton fab =
                (FloatingActionButton) getActivity().findViewById(R.id.fab_edit_task_done);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.saveTask(tvTitle.getText().toString(), tvComment.getText().toString(),
                                   cbIsComplete.isChecked(),
                                   arrayAdapter.getItem(spPriority.getSelectedItemPosition()),
                                   tvDueDate.getText().toString(), tvReminder.getText().toString());
            }
        });

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_delete_task:
                DeleteTasksDialogFragment dialogFragment = new DeleteTasksDialogFragment();
                dialogFragment.show(getChildFragmentManager(), "dialogDeleteTask");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle viewState = presenter.getViewState();
        viewState.putString(STATE_TITLE, tvTitle.getText().toString());
        viewState.putString(STATE_COMMENT, tvComment.getText().toString());
        viewState.putBoolean(STATE_COMPLETE, cbIsComplete.isChecked());
        viewState.putString(STATE_PRIORITY,
                            arrayAdapter.getItem(spPriority.getSelectedItemPosition()).name());
        viewState.putString(STATE_DUE_DATE, tvDueDate.getText().toString());
        viewState.putString(STATE_REMINDER, tvReminder.getText().toString());
    }

    @Override
    public void setPresenter(@NonNull TaskAddEditContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.clear();

        if (presenter.getTaskId() != null) {
            getActivity().getMenuInflater().inflate(R.menu.menu_taskaddedit, menu);
        }
    }

    @Override
    public void onDeleteTaskDialogPositiveClick() {
        presenter.deleteTask();
    }

    @Override
    public void onDeleteTaskDialogNegativeClick() {

    }

    @Override
    public void showEmptyTaskError() {
        Snackbar.make(tvTitle, "Task cannot be empty", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showTasksList() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void setComment(String comment) {
        tvComment.setText(comment);
    }

    @Override
    public void setIsComplete(boolean isComplete) {
        cbIsComplete.setChecked(isComplete);
    }

    @Override
    public void setPriority(String priority) {
        spPriority.setSelection(
                arrayAdapter.getPosition(Enum.valueOf(Task.Priority.class, priority)));
    }

    @Override
    public void setDueDate(long dueDate) {
        if (dueDate != 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
            tvDueDate.setText(dateFormat.format(new Date(dueDate)));
        } else {
            tvDueDate.setText(R.string.activity_taskaddedit_hint_duedate);
        }
    }

    @Override
    public void setReminder(long reminder) {
        if (reminder != 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm", Locale.US);
            tvReminder.setText(dateFormat.format(new Date(reminder)));
        } else {
            tvReminder.setText(R.string.activity_taskaddedit_hint_reminder);
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }
}