package com.romsvm.assistant.taskaddedit;

import com.romsvm.assistant.BasePresenter;
import com.romsvm.assistant.BaseView;
import com.romsvm.assistant.data.model.Task;

/**
 * Created by Roman on 28.02.2017.
 */

public interface TaskAddEditContract {
    interface View extends BaseView<TaskAddEditContract.Presenter> {
        void showEmptyTaskError();

        void showTasksList();

        void setTitle(String title);

        void setComment(String comment);

        void setIsComplete(boolean isComplete);

        void setPriority(String priority);

        void setDueDate(long dueDate);

        void setReminder(long reminder);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void saveTask(String title, String comment, boolean isComplete, Task.Priority priority,
                      String dueDate, String reminder);

        void deleteTask();

        void populateTask();

        String getTaskId();
    }
}