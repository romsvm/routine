package com.romsvm.assistant.selectedtasks.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksRepository;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 24.02.2017.
 */

public class GetAllProjects
        extends UseCase<GetAllProjects.RequestValues, GetAllProjects.ResponseValue> {

    public GetAllProjects(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = checkNotNull(tasksRepository, "tasksRepository cannot be null!");
    }

    private final TasksRepository tasksRepository;

    @Override
    protected void executeUseCase(final RequestValues values) {

        if (values.isForceUpdate()) {
            tasksRepository.refresh();
        }

        tasksRepository.getAllProjects(new TasksDataSource.GetAllProjectsCallback() {
            @Override
            public void onProjectsLoaded(List<Project> projects) {
                ResponseValue responseValue = new ResponseValue(projects);
                getUseCaseCallback().onSuccess(responseValue);
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });

        Log.d(UseCase.TAG, "GetAllProjects");
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final boolean forceUpdate;

        public RequestValues(boolean forceUpdate) {
            this.forceUpdate = forceUpdate;
        }

        public boolean isForceUpdate() {
            return forceUpdate;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final List<Project> projects;

        public ResponseValue(@NonNull List<Project> projects) {
            this.projects = checkNotNull(projects, "projects cannot be null!");
        }

        public List<Project> getProjects() {
            return projects;
        }
    }
}


