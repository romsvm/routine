package com.romsvm.assistant.selectedtasks;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.romsvm.assistant.Injection;
import com.romsvm.assistant.R;
import com.romsvm.assistant.util.ActivityUtils;
import com.romsvm.assistant.util.EspressoIdlingResource;

public class SelectedTasksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_tasks);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        SelectedTasksFragment selectedTasksFragment =
                (SelectedTasksFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.contentFrame);
        if (selectedTasksFragment == null) {
            // Create the fragment
            selectedTasksFragment = SelectedTasksFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), selectedTasksFragment, R.id.contentFrame);
        }

        SelectedTasksPresenter.getInstance(Injection.provideUseCaseHandler(), selectedTasksFragment,
                                           Injection.provideGetAllProjects(getApplicationContext()),
                                           Injection.provideGetAllTasks(getApplicationContext()),
                                           Injection.provideDeleteSelectedProjects(
                                                   getApplicationContext()), Injection
                                                   .provideGetTasksByFilter(
                                                           getApplicationContext()),
                                           Injection.provideExecLogout(getApplicationContext()));
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
