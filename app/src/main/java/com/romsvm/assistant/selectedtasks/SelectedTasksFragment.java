package com.romsvm.assistant.selectedtasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.romsvm.assistant.R;
import com.romsvm.assistant.data.auth.RoutineAuth;
import com.romsvm.assistant.data.filter.TasksFilterType;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.launch.LaunchActivity;
import com.romsvm.assistant.projectaddedit.ProjectAddEditActivity;
import com.romsvm.assistant.taskaddedit.TaskAddEditActivity;
import com.romsvm.assistant.util.DateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SelectedTasksFragment extends Fragment implements SelectedTasksContract.View,
                                                               DeleteProjectsDialogFragment.DeleteProjectsDialogListener {

    private static final int REQUEST_NEW_TASK = 0;
    private static final int REQUEST_NEW_PROJECT = 1;
    private static final int REQUEST_EDIT_PROJECT = 2;

    static final String STATE_DRAWER_IS_OPEN = "stateDrawerIsOpen";
    static final String STATE_PROJECTS_IS_OPEN = "stateProjectsIsOpen";
    static final String STATE_USER_NAME = "stateUserName";
    static final String STATE_FILTER_NAME = "stateFilterName";
    static final String STATE_FILTER_VALUE = "stateFilterValue";
    private static final String TAG = "SelectedTasksFragment";


    public static SelectedTasksFragment newInstance() {
        return new SelectedTasksFragment();
    }

    public SelectedTasksFragment() {
        // Required empty public constructor
    }

    private SelectedTasksContract.Presenter presenter;
    private TasksListAdapter tasksAdapter;
    private RecyclerView taskRecyclerView;
    FloatingActionButton fab;

    private RecyclerView navigationRecyclerView;
    private NavigationAdapter navigationAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tasksAdapter = new TasksListAdapter(new ArrayList<Task>(0));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        ArrayList<NavigationItem> data = new ArrayList<>();

        data.add(new NavigationItem(R.layout.drawer_header_view, null, null));
        data.add(new NavigationItem(R.layout.drawer_filter_view, TasksFilterType.BY_DATE,
                String.valueOf(DateUtils.getToday())));
        data.add(new NavigationItem(R.layout.drawer_filter_view, TasksFilterType.BY_DATE,
                String.valueOf(DateUtils.getTomorrow())));
        data.add(new NavigationItem(R.layout.drawer_filter_view, TasksFilterType.BY_DATE, null));
        data.add(new NavigationItem(R.layout.drawer_project_header_view,
                TasksFilterType.BY_PROJECT, null));
        data.add(new NavigationItem(R.layout.drawer_divider_view, null, null));
        data.add(new NavigationItem(R.layout.drawer_project_add_view, null, null));
        data.add(new NavigationItem(R.layout.drawer_settings_view, null, null));

        navigationRecyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerview);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        navigationRecyclerView.setLayoutManager(layoutManager);

        navigationAdapter = new NavigationAdapter(data);
        navigationRecyclerView.setAdapter(navigationAdapter);

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_selected_tasks, container, false);

        taskRecyclerView = (RecyclerView) root.findViewById(R.id.tasks_list);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        taskRecyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration
                .setDrawable(getContext().getResources().getDrawable(R.drawable.task_list_divider));
        taskRecyclerView.addItemDecoration(dividerItemDecoration);
        taskRecyclerView.setAdapter(tasksAdapter);

        // Set up floating action button
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab_add_task);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addNewTask();
            }
        });

        if (!presenter.getViewState().isEmpty()) {

            Bundle viewState = presenter.getViewState();

            presenter.setFilterName(viewState.getString(STATE_FILTER_NAME));
            presenter.setFilterValue(viewState.getString(STATE_FILTER_VALUE));

            if (viewState.getBoolean(STATE_DRAWER_IS_OPEN)) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout))
                        .openDrawer(Gravity.LEFT);
            }

            if (presenter.getFilterName().equals(TasksFilterType.BY_PROJECT.name())) {
                navigationAdapter.setupSelectedProject(presenter.getFilterValue());
            }

        } else {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(
                    RoutineAuth.FIREBASE_USER_UID + getString(R.string.file_shared_preferences),
                    Context.MODE_PRIVATE);

            String storedUseName = sharedPreferences.getString(STATE_USER_NAME, null);

            if (storedUseName != null) {
                RoutineAuth.USER_NAME = storedUseName;
            }

            String storedFilterName = sharedPreferences.getString(STATE_FILTER_NAME, null);
            String storedFilterValue = sharedPreferences.getString(STATE_FILTER_VALUE, null);

            if (storedFilterName != null) {
                presenter.setFilterName(storedFilterName);
                presenter.setFilterValue(storedFilterValue);
            }
        }

        presenter.start();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_selectedtasks, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.clear();

        if (presenter.getFilterName().equals(TasksFilterType.BY_PROJECT.name())) {
            getActivity().getMenuInflater().inflate(R.menu.menu_selectedtasks, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout))
                        .openDrawer(GravityCompat.START);
                return true;
            case R.id.item_delete_task:
                showDialog();
                return true;
            case R.id.item_edit:
                showProjectAddEdit((String) presenter.getFilterValue());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        /*Сохраняю в SharedPreferences имя и значение текущего фильтра*/
        saveFilterToSharedPreferences();
        presenter.getViewState().clear();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /*
        Сохранение вьюшки в презентере нужно только при перевороте, при выходе состояние вью стираю,
        чтобы оно не использовалось при заходе под другим пользователем
         */
        saveViewState();
    }

    private void saveFilterToSharedPreferences() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(
                RoutineAuth.FIREBASE_USER_UID + getString(R.string.file_shared_preferences),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(STATE_USER_NAME, RoutineAuth.USER_NAME);
        editor.putString(STATE_FILTER_NAME, presenter.getFilterName());
        editor.putString(STATE_FILTER_VALUE, presenter.getFilterValue());
        editor.apply();
    }

    private void saveViewState() {
        Bundle viewState = presenter.getViewState();
        viewState.putBoolean(STATE_DRAWER_IS_OPEN,
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout))
                        .isDrawerOpen(Gravity.LEFT));
        viewState.putString(STATE_FILTER_NAME, presenter.getFilterName());
        viewState.putString(STATE_FILTER_VALUE, presenter.getFilterValue());
    }

    @Override
    public void setPresenter(SelectedTasksContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showAddNewProject() {
        Intent intent = new Intent(getContext(), ProjectAddEditActivity.class);
        startActivityForResult(intent, REQUEST_NEW_PROJECT);
    }

    @Override
    public void showProjectsAndTasks(List<Project> projects, List<Task> tasks) {
        navigationAdapter.replaceModelData(projects, tasks);
        tasksAdapter.replaceData(tasks);
        getActivity().invalidateOptionsMenu();
    }

    private void showDialog() {
        DeleteProjectsDialogFragment dialogFragment = new DeleteProjectsDialogFragment();
        dialogFragment.show(getChildFragmentManager(), "dialogDeleteProjects");
    }

    @Override
    public void showProjectAddEdit(String projectId) {
        Intent intent = new Intent(getContext(), ProjectAddEditActivity.class);
        intent.putExtra(ProjectAddEditActivity.EXTRA_PROJECT_ID, projectId);
        startActivityForResult(intent, REQUEST_EDIT_PROJECT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == REQUEST_NEW_PROJECT || requestCode == REQUEST_EDIT_PROJECT) &&
            resultCode == Activity.RESULT_OK) {
            String newProjectId = data.getStringExtra("newProjectIdKey");
            presenter.setFilterName(TasksFilterType.BY_PROJECT.name());
            presenter.setFilterValue(newProjectId);
        }

        presenter.start();
    }

    @Override
    public void showAddNewTask() {
        Intent intent = new Intent(getContext(), TaskAddEditActivity.class);
        intent.putExtra(TaskAddEditActivity.EXTRA_FILTER_NAME, presenter.getFilterName());
        intent.putExtra(TaskAddEditActivity.EXTRA_FILTER_VALUE,
                presenter.getFilterValue());
        startActivityForResult(intent, REQUEST_NEW_TASK);
    }

    @Override
    public void showTasksForCurrentFilter(@NonNull List<Task> list) {
        tasksAdapter.replaceData(list);
    }

    @Override
    public void showTaskAddEdit(@NonNull Task task) {
        Intent intent = new Intent(getContext(), TaskAddEditActivity.class);
        intent.putExtra(TaskAddEditActivity.EXTRA_TASK_ID, task.getId());
        startActivityForResult(intent, REQUEST_NEW_TASK);
    }

    @Override
    public void showFilterName(@NonNull String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    @Override
    public void showExecLogout() {

        Intent intent = new Intent(this.getActivity(), LaunchActivity.class);

        this.getActivity().finish();
        this.getActivity().startActivity(intent);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onDeleteProjectDialogPositiveClick() {
        presenter.deleteSelectedProject();
    }

    @Override
    public void onDeleteProjectDialogNegativeClick() {

    }

    /*public class DividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable divider;

        public DividerItemDecoration() {

        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {


        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDraw(c, parent, state);
        }
    }*/

    private class TasksListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        class TaskViewHolder extends RecyclerView.ViewHolder {

            private TextView tvName;

            TaskViewHolder(View itemView) {
                super(itemView);

                this.tvName = (TextView) itemView.findViewById(R.id.title_task);
            }

            public void setName(String name) {
                tvName.setText(name);
            }

            public void setCompletion(boolean value) {

                if (value) {
                    tvName.setPaintFlags(tvName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    tvName.setPaintFlags(tvName.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
                }
            }
        }

        private List<Task> data;

        TasksListAdapter(List<Task> tasks) {
            this.data = tasks;
        }

        public void replaceData(List<Task> tasks) {
            this.data = tasks;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_task, parent, false);
            final RecyclerView.ViewHolder viewHolder = new TaskViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int adapterPosition = viewHolder.getAdapterPosition();
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        Task task = data.get(adapterPosition);
                        presenter.editTask(task);
                    }
                }
            });

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            Task task = data.get(position);
            ((TaskViewHolder) holder).setName(task.getTitle());
            ((TaskViewHolder) holder).setCompletion(task.getIsComplete());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    private class NavigationItem {

        private int viewType;
        private TasksFilterType filterType;
        private String filterValue;

        NavigationItem(int viewType, TasksFilterType filterType, String filterValue) {
            this.viewType = viewType;
            this.filterType = filterType;
            this.filterValue = filterValue;
        }

        int getViewType() {
            return viewType;
        }

        public TasksFilterType getFilterType() {
            return filterType;
        }

        public String getFilterValue() {
            return filterValue;
        }
    }

    private class NavigationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        class CustomViewHolder extends RecyclerView.ViewHolder {
            public CustomViewHolder(View itemView) {
                super(itemView);
            }

            public void setSelection(boolean value) {

            }
        }

        class HeaderViewHolder extends RecyclerView.ViewHolder {

            TextView tvUserName;

            HeaderViewHolder(View itemView) {
                super(itemView);

                tvUserName = (TextView) itemView.findViewById(R.id.user_name);
            }

            void setUserName(String name) {
                tvUserName.setText(name);
            }
        }

        class FilterViewHolder extends CustomViewHolder {

            private TextView tvName;
            private TextView tvAmountTask;
            private ImageView icon;
            private int defaultColorName;
            private int defaultColorAmountTask;
            private ColorFilter defaultColorFilter;

            FilterViewHolder(View itemView) {
                super(itemView);

                tvName = (TextView) itemView.findViewById(R.id.navigation_list_label_group);
                tvAmountTask = (TextView) itemView.findViewById(R.id.navigation_list_label_amount);
                icon = (ImageView) itemView.findViewById(R.id.navigation_list_image_group);

                defaultColorName = tvName.getCurrentTextColor();
                defaultColorAmountTask = tvAmountTask.getCurrentTextColor();
                defaultColorFilter = icon.getColorFilter();
            }

            public void setName(String name) {
                tvName.setText(name);
            }

            void setIcon(int id) {
                icon.setImageResource(id);
            }

            void setAmountTask(int amountTask) {
                tvAmountTask.setText(String.valueOf(amountTask));
            }

            @Override
            public void setSelection(boolean value) {
                super.setSelection(value);

                if (value) {
                    tvName.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tvAmountTask.setTextColor(getResources().getColor(R.color.colorPrimary));
                    icon.setColorFilter(getResources().getColor(R.color.colorPrimary));

                    itemView.setBackgroundColor(0x07000000);
                } else {
                    tvName.setTextColor(defaultColorName);
                    tvAmountTask.setTextColor(defaultColorAmountTask);
                    icon.setColorFilter(defaultColorFilter);

                    itemView.setBackgroundColor(0);
                }
            }
        }

        class DividerViewHolder extends RecyclerView.ViewHolder {

            DividerViewHolder(View itemView) {
                super(itemView);
            }
        }

        class SettingsViewHolder extends RecyclerView.ViewHolder {
            SettingsViewHolder(View itemView) {
                super(itemView);
            }
        }

        class ProjectHeaderViewHolder extends RecyclerView.ViewHolder {

            private ImageView expImage;

            ProjectHeaderViewHolder(View itemView) {
                super(itemView);

                expImage = (ImageView) itemView.findViewById(R.id.navigation_list_image_indicator);
                expImage.setImageResource(R.drawable.ic_navigation_indicator_down);
            }

            void setIsExpanded() {
                if (isProjectsHeaderExpanded) {
                    expImage.setImageResource(R.drawable.ic_navigation_indicator_up);
                } else {
                    expImage.setImageResource(R.drawable.ic_navigation_indicator_down);
                }
            }
        }

        class ProjectItemViewHolder extends CustomViewHolder {

            private TextView tvAmountTask;
            private TextView tvName;

            private int defaultColorName;
            private int defaultColorAmountTask;

            ProjectItemViewHolder(View itemView) {
                super(itemView);

                tvAmountTask = (TextView) itemView
                        .findViewById(R.id.navigation_exp_list_label_item_amount);
                tvName = (TextView) itemView.findViewById(R.id.navigation_exp_list_label_item);

                defaultColorName = tvName.getCurrentTextColor();
                defaultColorAmountTask = tvAmountTask.getCurrentTextColor();
            }

            void setAmountTask(int amountTask) {
                tvAmountTask.setText(String.valueOf(amountTask));
            }

            void setName(String name) {
                tvName.setText(name);
            }

            @Override
            public void setSelection(boolean value) {
                super.setSelection(value);

                if (value) {
                    tvName.setTextColor(getResources().getColor(R.color.colorPrimary));
                    tvAmountTask.setTextColor(getResources().getColor(R.color.colorPrimary));

                    itemView.setBackgroundColor(0x07000000);
                } else {
                    tvName.setTextColor(defaultColorName);
                    tvAmountTask.setTextColor(defaultColorAmountTask);

                    itemView.setBackgroundColor(0);
                }
            }
        }

        class ProjectAddViewHolder extends RecyclerView.ViewHolder {
            ProjectAddViewHolder(View itemView) {
                super(itemView);
            }
        }

        private List<NavigationItem> structureMenuData;
        private List<NavigationItem> structureProjectsData;
        private boolean isProjectsHeaderExpanded = false;
        private List<Project> projects;
        private List<Task> tasks;
        private HashMap<String, Integer> mapActionToAmountTask = new HashMap<>();
        private int positionSelectedItem = -1;

        NavigationAdapter(List<NavigationItem> structureMenuData) {
            this.structureMenuData = structureMenuData;
            this.structureProjectsData = new ArrayList<>();
        }

        void setupSelectedProject(String projectId) {
            isProjectsHeaderExpanded = true;
            structureMenuData.addAll(5, structureProjectsData);

            notifyDataSetChanged();
        }

        void replaceModelData(List<Project> projects, List<Task> tasks) {
            this.projects = projects;
            this.tasks = tasks;

            structureMenuData.removeAll(structureProjectsData);
            structureProjectsData.clear();

            mapActionToAmountTask.clear();

            for (Project project : projects) {
                structureProjectsData.add(new NavigationItem(R.layout.drawer_project_item_view,
                        TasksFilterType.BY_PROJECT,
                        project.getId()));
            }

            if (isProjectsHeaderExpanded) {
                structureMenuData.addAll(5, structureProjectsData);
            }


            for (NavigationItem item : structureMenuData) {
                if (item.getFilterType() == TasksFilterType.BY_DATE &&
                    item.getFilterValue() == null) {
                    mapActionToAmountTask
                            .put(TasksFilterType.BY_DATE.name() + item.getFilterValue(),
                                    tasks.size());
                } else if (item.getFilterType() == TasksFilterType.BY_DATE && item
                        .getFilterValue().equals(String.valueOf(DateUtils.getToday()))) {

                    int amountTask = 0;

                    for (Task task : this.tasks) {
                        if (task.getDueDate() == DateUtils.getToday()) {
                            amountTask++;
                        }
                    }

                    mapActionToAmountTask
                            .put(TasksFilterType.BY_DATE.name() + item.getFilterValue(),
                                    amountTask);

                } else if (item.getFilterType() == TasksFilterType.BY_DATE && item
                        .getFilterValue()
                        .equals(String.valueOf(DateUtils.getTomorrow()))) {


                    int amountTask = 0;
                    for (Task task : this.tasks) {
                        if (task.getDueDate() == DateUtils.getTomorrow()) {
                            amountTask++;
                        }
                    }
                    mapActionToAmountTask
                            .put(TasksFilterType.BY_DATE.name() + item.getFilterValue(),
                                    amountTask);
                }
            }

            for (NavigationItem item : structureProjectsData) {
                int amountTask = 0;

                for (Task task : tasks) {
                    if (task.getProjectId().equals(item.getFilterValue())) {
                        amountTask++;
                    }
                }

                mapActionToAmountTask.put((String) item.getFilterValue(), amountTask);
            }

            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return structureMenuData.get(position).getViewType();
        }

        private Project getProjectByID(String id) {
            for (Project project : projects) {
                if (project.getId().equals(id)) {
                    return project;
                }
            }

            return null;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view;
            final RecyclerView.ViewHolder viewHolder;

            switch (viewType) {
                case R.layout.drawer_header_view:
                    view = inflater.inflate(R.layout.drawer_header_view, parent, false);
                    viewHolder = new HeaderViewHolder(view);
                    break;

                case R.layout.drawer_filter_view:
                    view = inflater.inflate(R.layout.drawer_filter_view, parent, false);
                    viewHolder = new FilterViewHolder(view);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            int adapterPosition = viewHolder.getAdapterPosition();

                            if (adapterPosition != RecyclerView.NO_POSITION) {

                                notifyItemChanged(positionSelectedItem);
                                positionSelectedItem = viewHolder.getLayoutPosition();
                                notifyItemChanged(positionSelectedItem);

                                presenter.setFilterName(
                                        structureMenuData.get(adapterPosition).getFilterType()
                                                         .name());
                                presenter.setFilterValue(
                                        structureMenuData.get(adapterPosition).getFilterValue());

                                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout))
                                        .closeDrawers();
                                presenter.getTasksByFilter();

                                if (!presenter.getFilterName()
                                              .equals(TasksFilterType.BY_PROJECT.name())) {
                                    getActivity().invalidateOptionsMenu();
                                }

                                Log.d(TAG, "filter");
                            }
                        }
                    });
                    break;

                case R.layout.drawer_project_header_view:
                    view = inflater.inflate(R.layout.drawer_project_header_view, parent, false);
                    viewHolder = new ProjectHeaderViewHolder(view);

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "project header");

                            isProjectsHeaderExpanded = !isProjectsHeaderExpanded;

                            if (!isProjectsHeaderExpanded) {
                                structureMenuData.removeAll(structureProjectsData);
                                notifyDataSetChanged();
                            } else {
                                structureMenuData.addAll(5, structureProjectsData);
                                notifyDataSetChanged();
                            }

                            ((ProjectHeaderViewHolder) viewHolder).setIsExpanded();
                        }
                    });


                    break;

                case R.layout.drawer_project_item_view:
                    view = inflater.inflate(R.layout.drawer_project_item_view, parent, false);
                    viewHolder = new ProjectItemViewHolder(view);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            int adapterPosition = viewHolder.getAdapterPosition();

                            if (adapterPosition != RecyclerView.NO_POSITION) {
                                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout))
                                        .closeDrawers();

                                Project project = getProjectByID(
                                        (String) (structureMenuData.get(adapterPosition)
                                                                   .getFilterValue()));
                                presenter.setFilterName(TasksFilterType.BY_PROJECT.name());
                                presenter.setFilterValue(project.getId());
                                presenter.getTasksByFilter();

                                getActivity().invalidateOptionsMenu();

                                notifyItemChanged(positionSelectedItem);
                                positionSelectedItem = viewHolder.getLayoutPosition();
                                notifyItemChanged(positionSelectedItem);

                                Log.d(TAG, "project item");
                            }
                        }
                    });
                    break;

                case R.layout.drawer_project_add_view:
                    view = inflater.inflate(R.layout.drawer_project_add_view, parent, false);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout))
                                    .closeDrawers();
                            presenter.addNewProject();
                        }
                    });
                    viewHolder = new ProjectAddViewHolder(view);
                    break;

                case R.layout.drawer_divider_view:
                    view = inflater.inflate(R.layout.drawer_divider_view, parent, false);
                    viewHolder = new DividerViewHolder(view);
                    break;

                case R.layout.drawer_settings_view:
                    view = inflater.inflate(R.layout.drawer_settings_view, parent, false);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            presenter.execLogout();
                            Log.d(TAG, "settings");
                        }
                    });
                    viewHolder = new SettingsViewHolder(view);
                    break;
                default:
                    viewHolder = null;
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            if (holder instanceof CustomViewHolder) {
                ((CustomViewHolder) holder).setSelection(positionSelectedItem == position);
            }

            NavigationItem navigationItem = structureMenuData.get(position);

            if (holder instanceof FilterViewHolder) {
                FilterViewHolder filterViewHolder = (FilterViewHolder) holder;

                if (navigationItem.getFilterValue() == null) {
                    filterViewHolder.setName("All");
                    filterViewHolder.setIcon(R.drawable.ic_navigation_all);
                } else if (navigationItem.getFilterValue()
                                         .equals(String.valueOf(
                                                 DateUtils.getTomorrow()))) {
                    filterViewHolder.setName("Tomorrow");
                    filterViewHolder.setIcon(R.drawable.ic_navigation_tomorrow);
                } else if (navigationItem.getFilterValue()
                                         .equals(String.valueOf(DateUtils.getToday()))) {
                    filterViewHolder.setName("Today");
                    filterViewHolder.setIcon(R.drawable.ic_navigation_today);
                }

                Integer amountTask =
                        mapActionToAmountTask.get(structureMenuData.get(position).getFilterType()
                                                                   .name() +
                                                  navigationItem.getFilterValue());

                if (amountTask != null) {
                    filterViewHolder.setAmountTask(amountTask);
                } else {
                    filterViewHolder.setAmountTask(0);
                }

                if (navigationItem.getFilterValue() != null && navigationItem.getFilterValue().equals(presenter.getFilterValue())) {
                    filterViewHolder.setSelection(true);
                }

            } else if (holder instanceof HeaderViewHolder) {
                ((HeaderViewHolder) holder).setUserName(RoutineAuth.USER_NAME);
            } else if (holder instanceof ProjectItemViewHolder) {

                String projectId = (String) (structureMenuData.get(position).getFilterValue());
                String projectName = "ErrorBinding";

                for (Project project : projects) {
                    if (project.getId().equals(projectId)) {
                        projectName = project.getTitle();
                    }
                }

                ((ProjectItemViewHolder) holder)
                        .setAmountTask(mapActionToAmountTask.get(projectId));
                ((ProjectItemViewHolder) holder).setName(projectName);

                if (projectId.equals(presenter.getFilterValue())) {
                    ((ProjectItemViewHolder) holder).setSelection(true);
                }
            }
        }

        @Override
        public int getItemCount() {
            return structureMenuData.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }
    }
}