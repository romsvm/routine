package com.romsvm.assistant.selectedtasks;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.romsvm.assistant.R;

/**
 * Created by Roman on 09.03.2017.
 */

public class DeleteProjectsDialogFragment extends DialogFragment {
    public interface DeleteProjectsDialogListener {
        void onDeleteProjectDialogPositiveClick();

        void onDeleteProjectDialogNegativeClick();
    }

    private DeleteProjectsDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            FragmentManager supportFragmentManager =
                    ((SelectedTasksActivity) context).getSupportFragmentManager();
            Fragment fragment = supportFragmentManager.findFragmentById(R.id.contentFrame);

            listener = (DeleteProjectsDialogListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() + "must implement DeleteProjectsDialogListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.activity_selectedtasks_dialog_delete_selected_project);
        builder.setPositiveButton(R.string.activity_projects_delete_selected_projects_delete,
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialogInterface, int i) {
                                          listener.onDeleteProjectDialogPositiveClick();
                                      }
                                  });
        builder.setNegativeButton(R.string.activity_projects_delete_selected_projects_cancel,
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialogInterface, int i) {
                                          listener.onDeleteProjectDialogNegativeClick();
                                      }
                                  });

        return builder.create();
    }
}
