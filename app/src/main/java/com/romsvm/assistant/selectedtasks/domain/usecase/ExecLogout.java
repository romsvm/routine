package com.romsvm.assistant.selectedtasks.domain.usecase;

import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.auth.RoutineAuth;

/**
 * Created by Roman on 30.03.2017.
 */

public class ExecLogout extends UseCase<ExecLogout.RequestsValue, ExecLogout.ResponseValue> {

    private final RoutineAuth routineAuth;

    public ExecLogout(RoutineAuth routineAuth) {
        this.routineAuth = routineAuth;
    }

    @Override
    protected void executeUseCase(RequestsValue requestValues) {
        routineAuth.execLogout(getUseCaseCallback());
        Log.d(UseCase.TAG, "ExecLogout");
    }

    public static final class RequestsValue implements UseCase.RequestValues {
    }

    public static final class ResponseValue implements UseCase.ResponseValue {
    }

}
