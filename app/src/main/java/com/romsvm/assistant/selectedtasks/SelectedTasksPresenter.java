package com.romsvm.assistant.selectedtasks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.UseCaseHandler;
import com.romsvm.assistant.data.auth.RoutineAuth;
import com.romsvm.assistant.data.filter.TasksFilterType;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.projectaddedit.domain.usecase.DeleteProject;
import com.romsvm.assistant.selectedtasks.domain.usecase.ExecLogout;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetAllProjects;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetAllTasks;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetTasksByFilter;
import com.romsvm.assistant.util.DateUtils;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 23.02.2017.
 */

public class SelectedTasksPresenter implements SelectedTasksContract.Presenter {

    private static SelectedTasksPresenter instance;

    public static SelectedTasksPresenter getInstance(@NonNull UseCaseHandler useCaseHandler,
                                                     @NonNull SelectedTasksContract.View selectedTasksView,
                                                     @NonNull GetAllProjects getAllProjects,
                                                     @NonNull GetAllTasks getAllTasks,
                                                     @NonNull DeleteProject deleteProject,
                                                     @NonNull GetTasksByFilter getTasksByFilter,
                                                     @NonNull ExecLogout execLogout) {
        if (instance == null) {
            instance = new SelectedTasksPresenter(useCaseHandler, selectedTasksView, getAllProjects,
                    getAllTasks, deleteProject, getTasksByFilter,
                    execLogout);
        } else {
            instance.selectedTasksView = selectedTasksView;
        }

        instance.selectedTasksView.setPresenter(instance);

        return instance;
    }

    private SelectedTasksContract.View selectedTasksView;
    private final GetAllProjects getAllProjects;
    private final DeleteProject deleteProject;
    private final GetTasksByFilter getTasksByFilter;
    private final GetAllTasks getAllTasks;
    private final ExecLogout execLogout;
    private final UseCaseHandler useCaseHandler;

    private String filterName = TasksFilterType.BY_DATE.name();
    private String filterValue = null;

    private boolean isFirstLoad = true;
    private List<Project> projects = new ArrayList<>(0);
    private List<Task> tasks = new ArrayList<>(0);

    private Bundle viewState = new Bundle();

    private SelectedTasksPresenter(@NonNull UseCaseHandler useCaseHandler,
                                   @NonNull SelectedTasksContract.View selectedTasksView,
                                   @NonNull GetAllProjects getAllProjects,
                                   @NonNull GetAllTasks getAllTasks,
                                   @NonNull DeleteProject deleteProject,
                                   @NonNull GetTasksByFilter getTasksByFilter,
                                   @NonNull ExecLogout execLogout) {
        this.useCaseHandler = checkNotNull(useCaseHandler, "useCaseHandler can't be null");
        this.selectedTasksView =
                checkNotNull(selectedTasksView, "selectedTasksView cannot be null");
        this.getAllProjects = checkNotNull(getAllProjects, "getAllProjects cannot be null");
        this.getAllTasks = checkNotNull(getAllTasks, "getAllTasks cannot be null");
        this.deleteProject = checkNotNull(deleteProject, "deleteProject cannot be null");
        this.getTasksByFilter = checkNotNull(getTasksByFilter, "getTasksByFilter cannot be null");
        this.execLogout = checkNotNull(execLogout, "execLogout cannot be null");
    }

    @Override
    public void start() {

        Log.d("MYYSTAAAAAAAAAAAAART", "STAAAAAAAAAAAART");

        isFirstLoad = RoutineAuth.IS_FIRST_LOAD;
        getAllProjects();

        isFirstLoad = false;
        RoutineAuth.IS_FIRST_LOAD = false;
    }

    @Override
    public Bundle getViewState() {
        return viewState;
    }

    @Override
    public void getAllProjects() {
        GetAllProjects.RequestValues requestValues = new GetAllProjects.RequestValues(isFirstLoad);
        useCaseHandler.execute(getAllProjects, requestValues,
                new UseCase.UseCaseCallback<GetAllProjects.ResponseValue>() {
                    @Override
                    public void onSuccess(GetAllProjects.ResponseValue response) {
                        projects = response.getProjects();
                        if (selectedTasksView.isActive()) {
                            //selectedTasksView.showProjectsAndTasks(projects, tasks);
                            getAllTasks();
                        }
                    }

                    @Override
                    public void onError() {
                        projects.clear();
                        if (selectedTasksView.isActive()) {
                            //selectedTasksView.showProjectsAndTasks(projects, tasks);
                        }
                    }
                });
    }

    @Override
    public void getAllTasks() {
        GetAllTasks.RequestValue requestValues = new GetAllTasks.RequestValue();
        useCaseHandler.execute(getAllTasks, requestValues,
                new UseCase.UseCaseCallback<GetAllTasks.ResponseValue>() {
                    @Override
                    public void onSuccess(GetAllTasks.ResponseValue response) {
                        tasks = response.getTaskList();
                        if (selectedTasksView.isActive()) {
                            selectedTasksView.showProjectsAndTasks(projects, tasks);
                        }
                        getTasksByFilter();
                    }

                    @Override
                    public void onError() {
                        tasks.clear();
                        if (selectedTasksView.isActive()) {
                            selectedTasksView.showProjectsAndTasks(projects, tasks);
                        }

                        getTasksByFilter();
                    }
                });
    }

    @Override
    public void addNewProject() {
        selectedTasksView.showAddNewProject();
    }

    @Override
    public void getTasksByFilter() {
        useCaseHandler.execute(getTasksByFilter,
                new GetTasksByFilter.RequestValue(filterName, filterValue, isFirstLoad),
                new UseCase.UseCaseCallback<GetTasksByFilter.ResponseValue>() {
                    @Override
                    public void onSuccess(GetTasksByFilter.ResponseValue response) {
                        List<Task> taskList = response.getTaskList();
                        if (selectedTasksView.isActive()) {
                            selectedTasksView.showTasksForCurrentFilter(taskList);

                            if (filterName.equals(TasksFilterType.BY_PROJECT.name())) {
                                for (Project project : projects) {

                                    if (project.getId().equals(filterValue)) {
                                        selectedTasksView.showFilterName(project.getTitle());
                                        break;
                                    }
                                }
                            } else if (filterName.equals(TasksFilterType.BY_DATE.name())) {

                                if (filterValue != null) {
                                    if (filterValue.equals(String.valueOf(
                                            DateUtils.getToday()))) {
                                        selectedTasksView.showFilterName("Today");
                                    } else if (filterValue.equals(String.valueOf(
                                            DateUtils.getTomorrow()))) {
                                        selectedTasksView.showFilterName("Tomorrow");
                                    }
                                } else {
                                    selectedTasksView.showFilterName("All");
                                }
                            }
                        }
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    @Override
    public void deleteSelectedProject() {

        DeleteProject.RequestValues requestValues =
                new DeleteProject.RequestValues((String) filterValue);
        useCaseHandler.execute(deleteProject, requestValues,
                new UseCase.UseCaseCallback<DeleteProject.ResponseValue>() {
                    @Override
                    public void onSuccess(DeleteProject.ResponseValue response) {
                        filterName = TasksFilterType.BY_DATE.name();
                        filterValue = null;
                        start();
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void execLogout() {
        useCaseHandler.execute(execLogout, new ExecLogout.RequestsValue(),
                new UseCase.UseCaseCallback<ExecLogout.ResponseValue>() {
                    @Override
                    public void onSuccess(ExecLogout.ResponseValue response) {
                        if (selectedTasksView.isActive()) {
                            selectedTasksView.showExecLogout();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void addNewTask() {
        selectedTasksView.showAddNewTask();
    }

    @Override
    public void editTask(@NonNull Task clickedTask) {
        selectedTasksView.showTaskAddEdit(clickedTask);
    }

    @Override
    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    @Override
    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    @Override
    public String getFilterName() {
        return filterName;
    }

    @Override
    public String getFilterValue() {
        return filterValue;
    }
}