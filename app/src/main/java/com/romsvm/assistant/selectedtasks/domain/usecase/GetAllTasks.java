package com.romsvm.assistant.selectedtasks.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksRepository;

import java.util.List;

/**
 * Created by Roman on 16.03.2017.
 */

public class GetAllTasks extends UseCase<GetAllTasks.RequestValue, GetAllTasks.ResponseValue> {

    private final TasksRepository tasksRepository;

    public GetAllTasks(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }

    @Override
    protected void executeUseCase(final RequestValue values) {

        tasksRepository.getAllTasks(new TasksDataSource.GetAllTasksCallback() {
            @Override
            public void onTasksLoaded(List<Task> taskList) {
                if (taskList != null) {
                    ResponseValue responseValue = new ResponseValue(taskList);
                    getUseCaseCallback().onSuccess(responseValue);
                } else {
                    getUseCaseCallback().onError();
                }
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });

        Log.d(UseCase.TAG, "GetAllTasks");
    }

    public static final class RequestValue implements UseCase.RequestValues {

        public RequestValue() {
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private List<Task> taskList;

        public ResponseValue(List<Task> taskList) {
            this.taskList = taskList;
        }

        public List<Task> getTaskList() {
            return this.taskList;
        }
    }
}
