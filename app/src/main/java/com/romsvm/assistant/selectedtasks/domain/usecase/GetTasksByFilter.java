package com.romsvm.assistant.selectedtasks.domain.usecase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.filter.FilterFactory;
import com.romsvm.assistant.data.filter.SortType;
import com.romsvm.assistant.data.filter.TasksFilterType;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksRepository;

import java.util.List;

/**
 * Created by Roman on 16.03.2017.
 */

public class GetTasksByFilter
        extends UseCase<GetTasksByFilter.RequestValue, GetTasksByFilter.ResponseValue> {

    private final TasksRepository tasksRepository;

    public GetTasksByFilter(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }

    @Override
    protected void executeUseCase(final RequestValue values) {

        if (values.getIsForceUpdate()) {
            tasksRepository.refresh();
        }

        tasksRepository.getAllTasks(new TasksDataSource.GetAllTasksCallback() {
            @Override
            public void onTasksLoaded(List<Task> taskList) {
                if (taskList != null) {

                    List<Task> result = FilterFactory
                            .create(TasksFilterType.valueOf(getRequestValues().filterName))
                            .filter(taskList, getRequestValues().filterValue, false,
                                    SortType.CREATION_DATE);

                    ResponseValue responseValue = new ResponseValue(result);
                    getUseCaseCallback().onSuccess(responseValue);
                } else {
                    getUseCaseCallback().onError();
                }
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });

        Log.d(UseCase.TAG, "GetTasksByFilter");
    }

    public static final class RequestValue implements UseCase.RequestValues {

        private String filterName;
        private String filterValue;
        private boolean isForceUpdate;

        public RequestValue(@Nullable String filterName, @Nullable String filterValue,
                            boolean isForceUpdate) {
            this.filterName = filterName;
            this.filterValue = filterValue;
            this.isForceUpdate = isForceUpdate;
        }

        public String getFilterName() {
            return this.filterName;
        }

        public String getFilterValue() {
            return this.filterValue;
        }

        public boolean getIsForceUpdate() {
            return this.isForceUpdate;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private List<Task> taskList;

        public ResponseValue(List<Task> taskList) {
            this.taskList = taskList;
        }

        public List<Task> getTaskList() {
            return this.taskList;
        }
    }
}
