package com.romsvm.assistant.selectedtasks;

import com.romsvm.assistant.BasePresenter;
import com.romsvm.assistant.BaseView;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;

import java.util.List;

/**
 * Created by Roman on 23.02.2017.
 */

public interface SelectedTasksContract {
    interface View extends BaseView<Presenter> {
        //void setLoadingIndicator(boolean active);

        //Загружает список проектов в NavigationView
        void showProjectsAndTasks(List<Project> projects, List<Task> tasks);

        //Показывает в ActionBar имя фильтра (имя проекта)
        void showFilterName(String filterName);

        //Показывает список задач, полученных по фильтру
        void showTasksForCurrentFilter(List<Task> tasks);

        void showAddNewProject();

        void showProjectAddEdit(String projectId);

        //Запускает TaskAddEditActivity для создания новой задачи
        void showAddNewTask();

        //Запускает TaskAddEditActivity для редактирования выбранной задачи
        void showTaskAddEdit(Task task);

        void showExecLogout();

        boolean isActive();

		/*void showLoadingProjectsError();

		void showSuccessfullySavedMessage();*/
    }

    interface Presenter extends BasePresenter {

        //Методы устанавливают текущий фильтр
        void setFilterName(String filterName);

        void setFilterValue(String filterValue);

        String getFilterName();

        String getFilterValue();

        //загружает список проектов из сети
        void getAllProjects();

        //загружает список задач из сети
        void getAllTasks();

        //загружает из базы данный задачи, которые выбраны по текущему фильтру
        void getTasksByFilter();

        void addNewProject();

        void deleteSelectedProject();

        void addNewTask();

        void editTask(Task clickedTask);

        void execLogout();
    }
}
