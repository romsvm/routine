package com.romsvm.assistant.launch.domen.usecase;

import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.source.TasksRepository;

/**
 * Created by Roman on 09.04.2017.
 */

public class Sync extends UseCase<Sync.RequestValues, Sync.ResponseValue> {

    private TasksRepository tasksRepository;

    public Sync(TasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }

    @Override
    protected void executeUseCase(Sync.RequestValues requestValues) {
        tasksRepository.synchronize(getUseCaseCallback());

        Log.d(UseCase.TAG, "Sync");
    }

    public static final class RequestValues implements UseCase.RequestValues {

    }

    public static final class ResponseValue implements UseCase.ResponseValue {

    }

}
