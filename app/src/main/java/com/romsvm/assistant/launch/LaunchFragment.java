package com.romsvm.assistant.launch;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.romsvm.assistant.R;
import com.romsvm.assistant.selectedtasks.SelectedTasksActivity;

/**
 * Created by Roman on 28.03.2017.
 */

public class LaunchFragment extends Fragment implements LaunchContract.View, View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;

    public static LaunchFragment newInstance() {
        return new LaunchFragment();
    }


    private LaunchContract.Presenter presenter;
    private SignInButton button;

    public LaunchFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_launch, container, false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                presenter.signIn();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //Показываем кнопку только если нужна авторизация в showSignIn()
        button = (SignInButton) getActivity().findViewById(R.id.sign_in_button);
        button.setVisibility(View.INVISIBLE);

        presenter.start();
    }

    @Override
    public void signIn(GoogleApiClient googleApiClient) {
        //Авторизуюсь текущим клиентом
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                //Кнопка больше не нужна
                button.setVisibility(View.INVISIBLE);

                //Авторизуюсь в FireBase выбранным Google пользователем
                GoogleSignInAccount account = result.getSignInAccount();
                presenter.firebaseAuth(account);
            }
        }
    }

    @Override
    public void setPresenter(LaunchContract.Presenter presenter) {
        this.presenter = presenter;

    }

    @Override
    public void showSignIn() {
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(this);
    }

    @Override
    public void showLoadData() {
        presenter.loadData();
    }

    @Override
    public void showEndLoadData() {
        //TODO здесь вью может быть неактивным
        startActivity(new Intent(getActivity(), SelectedTasksActivity.class));
        getActivity().finish();
    }

    @Override
    public boolean isActive() {
        return this.isAdded();
    }
}
