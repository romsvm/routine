package com.romsvm.assistant.launch.domen.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.auth.RoutineAuth;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 30.03.2017.
 */

public class ExecLogin extends UseCase<ExecLogin.RequestValues, ExecLogin.ResponseValue> {

    private final RoutineAuth routineAuth;

    public ExecLogin(RoutineAuth routineAuth) {
        this.routineAuth = routineAuth;
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        GoogleSignInAccount account = requestValues.getAccount();
        routineAuth.execLogin(account, getUseCaseCallback());

        Log.d(UseCase.TAG, "ExecLogin");
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final GoogleSignInAccount account;

        public RequestValues(@NonNull GoogleSignInAccount account) {
            this.account = checkNotNull(account);
        }

        GoogleSignInAccount getAccount() {
            return account;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

    }
}
