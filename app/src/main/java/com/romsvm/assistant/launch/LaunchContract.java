package com.romsvm.assistant.launch;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;
import com.romsvm.assistant.BasePresenter;
import com.romsvm.assistant.BaseView;

/**
 * Created by Roman on 28.03.2017.
 */

public interface LaunchContract {

    interface View extends BaseView<Presenter> {
        void showLoadData();

        void showEndLoadData();

        void showSignIn();

        void signIn(GoogleApiClient googleApiClient);

        boolean isActive();
    }


    interface Presenter extends BasePresenter {
        void loadData();

        void sync();

        void signIn();

        void firebaseAuth(GoogleSignInAccount account);
    }
}