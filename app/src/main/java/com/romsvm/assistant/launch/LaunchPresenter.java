package com.romsvm.assistant.launch;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.UseCaseHandler;
import com.romsvm.assistant.data.auth.RoutineAuth;
import com.romsvm.assistant.launch.domen.usecase.CheckUser;
import com.romsvm.assistant.launch.domen.usecase.ExecLogin;
import com.romsvm.assistant.launch.domen.usecase.Sync;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetAllProjects;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetAllTasks;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 28.03.2017.
 */

public class LaunchPresenter implements LaunchContract.Presenter {

    private static LaunchPresenter instance;

    public static LaunchPresenter getInstance(UseCaseHandler useCaseHandler,
                                              LaunchContract.View launchView,
                                              GetAllProjects getAllProjects,
                                              GetAllTasks getAllTasks, CheckUser checkUser,
                                              ExecLogin execLogin, Sync sync) {
        if (instance == null) {
            instance = new LaunchPresenter(useCaseHandler, launchView, getAllProjects, getAllTasks,
                                           checkUser, execLogin, sync);
        } else {
            instance.launchView = launchView;
        }

        instance.launchView.setPresenter(instance);

        return instance;
    }

    private final UseCaseHandler useCaseHandler;
    private LaunchContract.View launchView;
    private final GetAllProjects getAllProjects;
    private final GetAllTasks getAllTasks;
    private final CheckUser checkUser;
    private final ExecLogin execLogin;
    private final Sync sync;

    public LaunchPresenter(@NonNull UseCaseHandler useCaseHandler,
                           @NonNull LaunchContract.View launchView,
                           @NonNull GetAllProjects getAllProjects, @NonNull GetAllTasks getAllTasks,
                           @NonNull CheckUser checkUser, @NonNull ExecLogin execLogin,
                           @NonNull Sync sync) {
        this.useCaseHandler = checkNotNull(useCaseHandler);
        this.launchView = checkNotNull(launchView);
        this.getAllProjects = checkNotNull(getAllProjects);
        this.getAllTasks = checkNotNull(getAllTasks);
        this.checkUser = checkNotNull(checkUser);
        this.execLogin = checkNotNull(execLogin);
        this.sync = checkNotNull(sync);
    }

    @Override
    public void start() {
        executeCheckUser();
    }

    private void executeCheckUser() {
        useCaseHandler.execute(checkUser, new CheckUser.RequestValues(),
                               new UseCase.UseCaseCallback<CheckUser.ResponseValue>() {
                                   @Override
                                   public void onSuccess(CheckUser.ResponseValue response) {

                                       if (!RoutineAuth.OFFLINE_MODE) {
                                           sync();
                                       } else {
                                           if (launchView.isActive()) {
                                               launchView.showLoadData();
                                           }
                                       }
                                   }

                                   @Override
                                   public void onError() {
                                       if (launchView.isActive()) {
                                           launchView.showSignIn();
                                       }
                                   }
                               });
    }

    @Override
    public void signIn() {
        if (launchView.isActive()) {
            launchView.signIn(RoutineAuth.getInstance(null).getGoogleApiClient());
        }
    }

    @Override
    public void firebaseAuth(GoogleSignInAccount account) {

        useCaseHandler.execute(execLogin, new ExecLogin.RequestValues(account),
                               new UseCase.UseCaseCallback<ExecLogin.ResponseValue>() {
                                   @Override
                                   public void onSuccess(ExecLogin.ResponseValue response) {
                                       executeCheckUser();
                                   }

                                   @Override
                                   public void onError() {

                                   }
                               });


    }

    @Override
    public void sync() {
        useCaseHandler.execute(sync, new Sync.RequestValues(),
                               new UseCase.UseCaseCallback<Sync.ResponseValue>() {
                                   @Override
                                   public void onSuccess(Sync.ResponseValue response) {
                                       if (launchView.isActive()) {
                                           launchView.showLoadData();
                                       }
                                   }

                                   @Override
                                   public void onError() {
                                       RoutineAuth.OFFLINE_MODE = true;
                                       if (launchView.isActive()) {
                                           launchView.showLoadData();
                                       }
                                   }
                               });
    }

    @Override
    public void loadData() {


        useCaseHandler.execute(getAllProjects, new GetAllProjects.RequestValues(true),
                               new UseCase.UseCaseCallback<GetAllProjects.ResponseValue>() {
                                   @Override
                                   public void onSuccess(GetAllProjects.ResponseValue response) {

                                   }

                                   @Override
                                   public void onError() {

                                   }
                               });

        useCaseHandler.execute(getAllTasks, new GetAllTasks.RequestValue(),
                               new UseCase.UseCaseCallback<GetAllTasks.ResponseValue>() {
                                   @Override
                                   public void onSuccess(GetAllTasks.ResponseValue response) {
                                       if (launchView.isActive()) {
                                           launchView.showEndLoadData();
                                       }
                                   }

                                   @Override
                                   public void onError() {

                                   }
                               });
    }

    @Override
    public Bundle getViewState() {
        return null;
    }


}
