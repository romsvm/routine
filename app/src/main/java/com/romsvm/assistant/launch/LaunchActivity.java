package com.romsvm.assistant.launch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.romsvm.assistant.Injection;
import com.romsvm.assistant.R;
import com.romsvm.assistant.util.ActivityUtils;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        LaunchFragment launchFragment =
                (LaunchFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (launchFragment == null) {
            launchFragment = LaunchFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), launchFragment,
                                                R.id.contentFrame);
        }

        LaunchPresenter.getInstance(Injection.provideUseCaseHandler(), launchFragment,
                                    Injection.provideGetAllProjects(getApplicationContext()),
                                    Injection.provideGetAllTasks(getApplicationContext()),
                                    Injection.provideCheckUser(getApplicationContext()),
                                    Injection.provideExecAuthorization(getApplicationContext()),
                                    Injection.provideSync(getApplicationContext()));
    }
}
