package com.romsvm.assistant.projectaddedit;

import com.romsvm.assistant.BasePresenter;
import com.romsvm.assistant.BaseView;

/**
 * Created by Roman on 25.02.2017.
 */

public interface ProjectAddEditContract {
    interface View extends BaseView<ProjectAddEditContract.Presenter> {
        void showEmptyProjectError();

        void showProjectsList(String newProjectId);

        void setTitle(String title);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void saveProject(String title);

        void populateProject();
    }
}