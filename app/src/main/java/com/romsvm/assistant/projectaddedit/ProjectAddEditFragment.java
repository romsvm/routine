package com.romsvm.assistant.projectaddedit;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.romsvm.assistant.R;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectAddEditFragment extends Fragment implements ProjectAddEditContract.View {
    public static final String ARGUMENT_EDIT_PROJECT_ID = "EDIT_PROJECT_ID";

    private static final String STATE_PROJECT_NAME = "stateProjectName";

    public static ProjectAddEditFragment newInstance() {
        return new ProjectAddEditFragment();
    }

    public ProjectAddEditFragment() {
        // Required empty public constructor
    }

    private TextView titleTV;
    private ProjectAddEditContract.Presenter presenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton fab =
                (FloatingActionButton) getActivity().findViewById(R.id.fab_edit_project_done);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.saveProject(titleTV.getText().toString());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_project_add_edit, container, false);
        titleTV = (TextView) root.findViewById(R.id.add_project_title);

        if (!presenter.getViewState().isEmpty()) {

            Bundle viewState = presenter.getViewState();
            titleTV.setText(viewState.getString(STATE_PROJECT_NAME));
            viewState.clear();
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle viewState = presenter.getViewState();
        viewState.putString(STATE_PROJECT_NAME, titleTV.getText().toString());
    }

    @Override
    public void setPresenter(@NonNull ProjectAddEditContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Override
    public void showEmptyProjectError() {
        Snackbar.make(titleTV, "Project cannot be empty", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProjectsList(String newProjectId) {
        Intent intent = new Intent();
        intent.putExtra("newProjectIdKey", newProjectId);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void setTitle(String title) {
        titleTV.setText(title);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

}
