package com.romsvm.assistant.projectaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 25.02.2017.
 */

public class GetProject extends UseCase<GetProject.RequestValues, GetProject.ResponseValue> {
    public GetProject(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    private final TasksRepository tasksRepository;

    @Override
    protected void executeUseCase(final RequestValues values) {
        tasksRepository.getProject(values.getProjectId(), new TasksDataSource.GetProjectCallback() {
            @Override
            public void onProjectLoaded(Project project) {
                if (project != null) {
                    ResponseValue responseValue = new ResponseValue(project);
                    getUseCaseCallback().onSuccess(responseValue);
                } else {
                    getUseCaseCallback().onError();
                }
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });

        Log.d(UseCase.TAG, "GetProject");
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final String projectId;

        public RequestValues(@NonNull String projectId) {
            this.projectId = checkNotNull(projectId, "projectId cannot be null!");
        }

        public String getProjectId() {
            return projectId;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private Project project;

        public ResponseValue(@NonNull Project project) {
            this.project = checkNotNull(project, "project cannot be null!");
        }

        public Project getProject() {
            return project;
        }
    }
}
