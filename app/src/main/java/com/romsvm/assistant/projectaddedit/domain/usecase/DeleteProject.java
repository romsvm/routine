package com.romsvm.assistant.projectaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.source.TasksRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 25.02.2017.
 */

public class DeleteProject
        extends UseCase<DeleteProject.RequestValues, DeleteProject.ResponseValue> {
    public DeleteProject(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    private final TasksRepository tasksRepository;

    @Override
    protected void executeUseCase(final RequestValues values) {
        tasksRepository.deleteProject(values.getProjectId());
        getUseCaseCallback().onSuccess(new ResponseValue());

        Log.d(UseCase.TAG, "DeleteProject");
    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final String projectId;

        public RequestValues(@NonNull String projectId) {
            this.projectId = checkNotNull(projectId, "projectId cannot be null!");
        }

        public String getProjectId() {
            return projectId;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {
    }
}
