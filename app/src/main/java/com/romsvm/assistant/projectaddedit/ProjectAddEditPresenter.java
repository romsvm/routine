package com.romsvm.assistant.projectaddedit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.UseCaseHandler;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.projectaddedit.domain.usecase.GetProject;
import com.romsvm.assistant.projectaddedit.domain.usecase.SaveProject;
import com.romsvm.assistant.projectaddedit.domain.usecase.UpdateProject;

import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 25.02.2017.
 */

public class ProjectAddEditPresenter implements ProjectAddEditContract.Presenter {

    private static ProjectAddEditPresenter instance;

    public static ProjectAddEditPresenter getInstance(@NonNull UseCaseHandler useCaseHandler,
                                                      @Nullable String projectId,
                                                      @NonNull ProjectAddEditContract.View projectAddEditView,
                                                      @NonNull GetProject getProject,
                                                      @NonNull SaveProject saveProject,
                                                      @NonNull UpdateProject updateProject) {
        if (instance == null) {
            instance = new ProjectAddEditPresenter(useCaseHandler, projectId, projectAddEditView,
                                                   getProject, saveProject, updateProject);
        } else {
            instance.projectId = projectId;
            instance.projectAddEditView = projectAddEditView;
        }

        instance.projectAddEditView.setPresenter(instance);

        return instance;
    }

    private ProjectAddEditContract.View projectAddEditView;
    private final GetProject getProject;
    private final SaveProject saveProject;
    private final UpdateProject updateProject;
    private final UseCaseHandler useCaseHandler;

    @Nullable
    private String projectId;

    private Bundle viewState = new Bundle();

    private ProjectAddEditPresenter(@NonNull UseCaseHandler useCaseHandler,
                                    @Nullable String projectId,
                                    @NonNull ProjectAddEditContract.View projectAddEditView,
                                    @NonNull GetProject getProject,
                                    @NonNull SaveProject saveProject,
                                    @NonNull UpdateProject updateProject) {
        this.useCaseHandler = checkNotNull(useCaseHandler, "useCaseHandler cannot be null");
        this.projectId = projectId;
        this.projectAddEditView =
                checkNotNull(projectAddEditView, "projectAddEditView cannot be null");
        this.getProject = checkNotNull(getProject, "getTask cannot be null");
        this.saveProject = checkNotNull(saveProject, "saveProject cannot be null");
        this.updateProject = checkNotNull(updateProject, "updateProject cannot be null");

        this.projectAddEditView.setPresenter(this);
    }

    @Override
    public void start() {
        if (projectId != null) {
            populateProject();
        }
    }

    @Override
    public Bundle getViewState() {
        return viewState;
    }

    @Override
    public void saveProject(String title) {
        if (isNewProject()) {
            createProject(title);
        } else {
            updateProject(title);
        }
    }

    @Override
    public void populateProject() {
        if (projectId == null) {
            throw new RuntimeException("populateTask() was called but project is new");
        }

        useCaseHandler.execute(getProject, new GetProject.RequestValues(projectId),
                               new UseCase.UseCaseCallback<GetProject.ResponseValue>() {
                                   @Override
                                   public void onSuccess(GetProject.ResponseValue response) {
                                       showProject(response.getProject());
                                   }

                                   @Override
                                   public void onError() {
                                       showEmptyProjectError();
                                   }
                               });
    }

    private void showProject(Project project) {
        if (projectAddEditView.isActive()) {
            projectAddEditView.setTitle(project.getTitle());
        }
    }

    private void showSaveError() {

    }

    private void showEmptyProjectError() {
        if (projectAddEditView.isActive()) {
            projectAddEditView.showEmptyProjectError();
        }
    }

    private boolean isNewProject() {
        return projectId == null;
    }

    private void createProject(String title) {
        final Project newProject = new Project(title);

        if (newProject.isEmpty()) {
            projectAddEditView.showEmptyProjectError();
        } else {
            useCaseHandler.execute(saveProject, new SaveProject.RequestValues(newProject),
                                   new UseCase.UseCaseCallback<SaveProject.ResponseValue>() {
                                       @Override
                                       public void onSuccess(SaveProject.ResponseValue response) {
                                           if (projectAddEditView.isActive()) {
                                               projectAddEditView
                                                       .showProjectsList(newProject.getId());
                                           }
                                       }

                                       @Override
                                       public void onError() {
                                           showSaveError();
                                       }
                                   });
        }
    }

    private void updateProject(final String title) {

        if (projectId == null) {
            throw new RuntimeException("populateTask() was called but project is new");
        }

        useCaseHandler.execute(getProject, new GetProject.RequestValues(projectId),
                               new UseCase.UseCaseCallback<GetProject.ResponseValue>() {
                                   @Override
                                   public void onSuccess(GetProject.ResponseValue response) {
                                       Project oldProject = response.getProject();
                                       final Project newProject =
                                               new Project(oldProject.getId(), title,
                                                           oldProject.getDateCreate(),
                                                           new Date().getTime());

                                       ProjectAddEditPresenter.this.useCaseHandler
                                               .execute(ProjectAddEditPresenter.this.updateProject,
                                                        new UpdateProject.RequestValues(newProject),
                                                        new UseCase.UseCaseCallback<UpdateProject.ResponseValue>() {
                                                            @Override
                                                            public void onSuccess(
                                                                    UpdateProject.ResponseValue response) {
                                                                if (projectAddEditView.isActive()) {
                                                                    projectAddEditView
                                                                            .showProjectsList(
                                                                                    newProject
                                                                                            .getId());
                                                                }
                                                            }

                                                            @Override
                                                            public void onError() {

                                                            }
                                                        });
                                   }

                                   @Override
                                   public void onError() {
                                       showEmptyProjectError();
                                   }
                               });
    }
}
