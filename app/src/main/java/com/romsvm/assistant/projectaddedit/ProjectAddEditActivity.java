package com.romsvm.assistant.projectaddedit;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.romsvm.assistant.Injection;
import com.romsvm.assistant.R;
import com.romsvm.assistant.util.ActivityUtils;
import com.romsvm.assistant.util.EspressoIdlingResource;

public class ProjectAddEditActivity extends AppCompatActivity {
    public static final int REQUEST_ADD_PROJECT = 1;
    public static final String EXTRA_PROJECT_ID = "PROJECT_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_add_edit);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        ProjectAddEditFragment projectAddEditFragment =
                (ProjectAddEditFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.contentFrame);

        String projectId = getIntent().getStringExtra(ProjectAddEditActivity.EXTRA_PROJECT_ID);

        if (projectAddEditFragment == null) {
            projectAddEditFragment = ProjectAddEditFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), projectAddEditFragment,
                                                R.id.contentFrame);
        }

        if (projectId == null) {
            this.setTitle(R.string.title_activity_project_add);
        } else {
            this.setTitle(R.string.title_activity_project_edit);
        }

        ProjectAddEditPresenter
                .getInstance(Injection.provideUseCaseHandler(), projectId, projectAddEditFragment,
                             Injection.provideGetProject(getApplicationContext()),
                             Injection.provideSaveProject(getApplicationContext()),
                             Injection.provideUpdateProject(getApplicationContext()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }

}
