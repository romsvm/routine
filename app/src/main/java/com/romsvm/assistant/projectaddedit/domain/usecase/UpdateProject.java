package com.romsvm.assistant.projectaddedit.domain.usecase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.source.TasksRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 01.03.2017.
 */

public class UpdateProject
        extends UseCase<UpdateProject.RequestValues, UpdateProject.ResponseValue> {
    public UpdateProject(@NonNull TasksRepository tasksRepository) {
        this.tasksRepository = checkNotNull(tasksRepository);
    }

    private final TasksRepository tasksRepository;

    @Override
    protected void executeUseCase(final UpdateProject.RequestValues values) {
        Project project = values.getProject();
        this.tasksRepository.updateProject(project);

        getUseCaseCallback().onSuccess(new UpdateProject.ResponseValue(project));

        Log.d(UseCase.TAG, "UpdateProject");
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final Project project;

        public RequestValues(@NonNull Project project) {
            this.project = checkNotNull(project, "project cannot be null!");
        }

        public Project getProject() {
            return project;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final Project project;

        public ResponseValue(@NonNull Project project) {
            this.project = checkNotNull(project, "project cannot be null!");
        }

        public Project getProject() {
            return project;
        }
    }
}
