package com.romsvm.assistant;

import android.os.Bundle;

/**
 * Created by Roman on 23.02.2017.
 */

public interface BasePresenter {
    void start();

    Bundle getViewState();
}
