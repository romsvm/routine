package com.romsvm.assistant.data.source.remote;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.auth.RoutineAuth;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksPersistenceContract;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Только для обучения, вообще тут сервис не нужен, а можно работать сразу с http клиентом, т.к.
 * useCase уже выполняется в отдельном потоке
 */

public class TasksRemoteDataSource implements TasksDataSource {

    interface FirebaseAPI {

        @GET("projects.json")
        Call<Map<String, Project>> getAllProjects();

        @GET("projects/{projectId}.json")
        Call<Project> getProject(@Path("projectId") String id);

        @DELETE("projects.json")
        Call<ResponseBody> deleteAllProjects();

        @DELETE("projects/{projectId}.json")
        Call<ResponseBody> deleteProject(@Path("projectId") String projectId);

        @PUT("projects/{projectId}.json")
        Call<Project> saveProject(@Path("projectId") String projectId, @Body Project project);

        @PUT("projects/{projectId}.json")
        Call<Project> updateProject(@Path("projectId") String projectId, @Body Project project);

        @GET("tasks.json")
        Call<Map<String, Task>> getAllTasks();

        @GET("tasks.json")
        Call<Map<String, Task>> getTasksByProjectId(@Query("orderBy") String projectId,
                                                    @Query("equalTo") String valueProjectId);

        @DELETE("tasks.json")
        Call<ResponseBody> deleteAllTasks();

        @DELETE("tasks/{taskId}.json")
        Call<ResponseBody> deleteTask(@Path("taskId") String taskId);

        @PUT("tasks/{taskId}.json")
        Call<Project> saveTask(@Path("taskId") String taskId, @Body Task task);

        @PUT("tasks/{taskId}.json")
        Call<Project> updateTask(@Path("taskId") String taskId, @Body Task task);
    }

    private static FirebaseAPI FIREBASE_API;
    private static TasksRemoteDataSource INSTANCE;

    public static TasksRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TasksRemoteDataSource();
        }

        return INSTANCE;
    }

    private Retrofit retrofit;

    private TasksRemoteDataSource() {

    }

    public void initHttpClient(String baseURL) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okHttpClient =
                new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();
                        HttpUrl originalHttpUrl = originalRequest.url();
                        HttpUrl newHttpUrl = originalHttpUrl.newBuilder()
                                                            .addQueryParameter("auth", RoutineAuth.TOKEN_FIREBASE).build();

                        Request.Builder builder = originalRequest.newBuilder().url(newHttpUrl);
                        Request newRequest = builder.build();

                        return chain.proceed(newRequest);
                    }
                }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        FIREBASE_API = retrofit.create(FirebaseAPI.class);
    }

    @Override
    public void synchronize(UseCase.UseCaseCallback callback) {

    }



    @Override
    public void getAllProjects(@NonNull GetAllProjectsCallback callback) {
        Call<Map<String, Project>> call = TasksRemoteDataSource.FIREBASE_API.getAllProjects();

        try {
            Response<Map<String, Project>> response = call.execute();

            if (response.isSuccessful()) {
                Map<String, Project> map = response.body();

                ArrayList<Project> arrayList = new ArrayList<>(0);

                if (map != null) {
                    arrayList.addAll(map.values());

                    Collections.sort(arrayList, new Comparator<Project>() {
                        @Override
                        public int compare(Project a, Project b) {

                            int result = 0;

                            if (a.getDateCreate() > b.getDateCreate()) {
                                result = 1;
                            } else if (a.getDateCreate() < b.getDateCreate()) {
                                result = -1;
                            }

                            return result;
                        }
                    });
                }

                callback.onProjectsLoaded(arrayList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getProject(@NonNull String projectId, @NonNull GetProjectCallback callback) {

    }

    @Override
    public void deleteAllProjects() {
        Call call = FIREBASE_API.deleteAllProjects();
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteProject(@NonNull String projectId) {
        Call call = FIREBASE_API.deleteProject(projectId);
        try {
            call.execute();

            Call<Map<String, Task>> callGetAllTasks = FIREBASE_API.getTasksByProjectId(
                    "\"" + TasksPersistenceContract.TaskEntry.COLUMN_NAME_PROJECT_ID + "\"",
                    "\"" + projectId + "\"");
            Response<Map<String, Task>> response = callGetAllTasks.execute();

            if (response.isSuccessful()) {
                Map<String, Task> map = response.body();
                ArrayList<Task> arrayList = new ArrayList<>(0);

                if (map != null) {
                    arrayList.addAll(map.values());

                    for (Task task : arrayList) {
                        Call callDeleteTask = FIREBASE_API.deleteTask(task.getId());
                        callDeleteTask.execute();
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveProject(@NonNull Project project) {
        Call<Project> call = FIREBASE_API.saveProject(project.getId(), project);
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateProject(@NonNull Project project) {
        Call<Project> call = FIREBASE_API.updateProject(project.getId(), project);
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTask(@NonNull String taskId) {
        Call call = TasksRemoteDataSource.FIREBASE_API.deleteTask(taskId);
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllTasks() {
        Call call = TasksRemoteDataSource.FIREBASE_API.deleteAllTasks();
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAll() {
        deleteAllProjects();
        deleteAllTasks();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void getAllTasks(@NonNull GetAllTasksCallback callback) {
        Call<Map<String, Task>> call = FIREBASE_API.getAllTasks();

        Response<Map<String, Task>> response = null;
        try {
            response = call.execute();

            if (response.isSuccessful()) {
                Map<String, Task> map = response.body();
                ArrayList<Task> arrayList = new ArrayList<>(0);

                if (map != null) {
                    arrayList.addAll(map.values());

                    Collections.sort(arrayList, new Comparator<Task>() {
                        @Override
                        public int compare(Task a, Task b) {

                            int result = 0;

                            if (a.getDateCreate() > b.getDateCreate()) {
                                result = 1;
                            } else if (a.getDateCreate() < b.getDateCreate()) {
                                result = -1;
                            }

                            return result;
                        }
                    });
                }

                callback.onTasksLoaded(arrayList);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void getTask(@NonNull String taskId, @NonNull GetTaskCallback callback) {

    }

    @Override
    public void getTasksByFilter(String filterName, String filterValue,
                                 @NonNull GetTasksByFilterCallback callback) {

    }

    @Override
    public void saveTask(@NonNull Task task) {
        Call call = TasksRemoteDataSource.FIREBASE_API.saveTask(task.getId(), task);
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSelectedTasks(@NonNull List<Task> taskList) {
        for (Task task : taskList) {
            Call call = TasksRemoteDataSource.FIREBASE_API.updateTask(task.getId(), task);
            try {
                call.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
