package com.romsvm.assistant.data.source;

import android.support.annotation.NonNull;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;

import java.util.List;

/**
 * Created by Roman on 24.02.2017.
 */

public interface TasksDataSource {


    interface GetAllProjectsCallback {
        void onProjectsLoaded(List<Project> projects);

        void onDataNotAvailable();
    }

    interface GetProjectCallback {
        void onProjectLoaded(Project project);

        void onDataNotAvailable();
    }

    interface GetAllTasksCallback {
        void onTasksLoaded(List<Task> tasks);

        void onDataNotAvailable();
    }

    interface GetTaskCallback {
        void onTaskLoaded(Task task);

        void onDataNotAvailable();
    }

    interface GetTasksByFilterCallback {
        void onTasksLoaded(List<Task> taskList);

        void onDataNotAvailable();
    }

    void synchronize(UseCase.UseCaseCallback callback);

    void refresh();

    void getAllProjects(@NonNull GetAllProjectsCallback callback);

    void getProject(@NonNull String projectId, @NonNull GetProjectCallback callback);

    void deleteProject(@NonNull String projectId);

    void saveProject(@NonNull Project project);

    void updateProject(@NonNull Project project);

    void deleteAllProjects();

    void getAllTasks(@NonNull GetAllTasksCallback callback);

    void getTask(@NonNull String taskId, @NonNull GetTaskCallback callback);

    void getTasksByFilter(String filterName, String filterValue,
                          @NonNull GetTasksByFilterCallback callback);

    void deleteTask(@NonNull String taskId);

    void saveTask(@NonNull Task task);

    void updateSelectedTasks(@NonNull List<Task> taskList);

    void deleteAllTasks();

    void deleteAll();

}
