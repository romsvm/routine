package com.romsvm.assistant.data.filter;

/**
 * Created by Roman on 11.04.2017.
 */

public enum SortType {
    CREATION_DATE,
    DUE_DATE,
    PRIORITY
}
