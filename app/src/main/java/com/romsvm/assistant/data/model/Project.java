package com.romsvm.assistant.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Strings;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Roman on 16.02.2017.
 */

public class Project implements Parcelable {

    public static final Creator<Project> CREATOR = new Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel parcel) {
            return new Project(parcel);
        }

        @Override
        public Project[] newArray(int i) {
            return new Project[i];
        }
    };

    @NonNull
    private String id;

    @Nullable
    private String title;

    private long dateCreate;
    private long dateUpdate;

    public Project(@Nullable String title) {
        this.title = title;
        id = UUID.randomUUID().toString();
        dateCreate = new Date().getTime();
        dateUpdate = dateCreate;
    }

    public Project(@NonNull String id, @Nullable String title, long dateCreate, long dateUpdate) {
        this.id = id;
        this.title = title;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    public Project(Parcel source) {
        this.id = source.readString();
        this.title = source.readString();
        this.dateCreate = source.readLong();
        this.dateUpdate = source.readLong();
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public long getDateCreate() {
        return dateCreate;
    }

    public long getDateUpdate() {
        return dateUpdate;
    }

    public boolean isEmpty() {
        return Strings.isNullOrEmpty(title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.title);
        parcel.writeLong(this.dateCreate);
        parcel.writeLong(this.dateUpdate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        return this.toString().equals(o.toString());
    }

    @Override
    public String toString() {
        return this.title + " id " + this.id + " dateCreate " + dateCreate + " dateUpdate " + dateUpdate;
    }
}


