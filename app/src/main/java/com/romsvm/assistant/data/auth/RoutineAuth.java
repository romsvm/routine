package com.romsvm.assistant.data.auth;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.romsvm.assistant.R;
import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.source.remote.TasksRemoteDataSource;
import com.romsvm.assistant.launch.domen.usecase.CheckUser;
import com.romsvm.assistant.launch.domen.usecase.ExecLogin;
import com.romsvm.assistant.selectedtasks.domain.usecase.ExecLogout;

/**
 * Created by Roman on 30.03.2017.
 */

public class RoutineAuth implements GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "RoutineAuth";

    public static String FIREBASE_USER_UID;
    public static String TOKEN_FIREBASE;
    public static String BASE_URL;
    public static String USER_NAME = "NoName";
    public static boolean OFFLINE_MODE = false;
    public static boolean IS_FIRST_LOAD = true;
    private static RoutineAuth INSTANCE;

    public static RoutineAuth getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new RoutineAuth(context);
        }

        return INSTANCE;
    }

    private FirebaseAuth firebaseAuth;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleApiClient googleApiClient;

    private RoutineAuth(Context context) {
        firebaseAuth = FirebaseAuth.getInstance();

        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //web application client_id из google-services.json для доступа в Oauth 2.0
                .requestIdToken(context.getString(R.string.client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .addConnectionCallbacks(this)
                .build();


    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public void checkUser(final UseCase.UseCaseCallback useCaseCallback) {

        RoutineAuth.IS_FIRST_LOAD = false;

        if (firebaseAuth.getCurrentUser() != null) {

            firebaseAuth.getCurrentUser().getToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {

                    try {
                        RoutineAuth.TOKEN_FIREBASE = task.getResult().getToken();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d(TAG, "Offline");
                        OFFLINE_MODE = true;
                    }

                    useCaseCallback.onSuccess(new CheckUser.ResponseValue());
                }
            });

            RoutineAuth.FIREBASE_USER_UID = firebaseAuth.getCurrentUser().getUid();
            RoutineAuth.BASE_URL = "https://assistant-11c55.firebaseio.com/" + RoutineAuth.FIREBASE_USER_UID + "/";
            TasksRemoteDataSource.getInstance().initHttpClient(RoutineAuth.BASE_URL);
        } else {
            useCaseCallback.onError();
        }
    }

    public void execLogin(GoogleSignInAccount account, final UseCase.UseCaseCallback useCaseCallback) {

        USER_NAME = account.getEmail().split("@")[0];

        firebaseAuth = FirebaseAuth.getInstance();

        Log.d(TAG, "firebaseAuthInAccount: " + account.getId());
        Log.d(TAG, "token: " + account.getIdToken());

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                if (!task.isSuccessful()) {
                    Log.w(TAG, "signInWithCredential", task.getException());
                    useCaseCallback.onError();
                } else {

                    RoutineAuth.FIREBASE_USER_UID = firebaseAuth.getCurrentUser().getUid();
                    RoutineAuth.BASE_URL = "https://assistant-11c55.firebaseio.com/" + RoutineAuth.FIREBASE_USER_UID + "/";
                    TasksRemoteDataSource.getInstance().initHttpClient(RoutineAuth.BASE_URL);

                    firebaseAuth.getCurrentUser().getToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                        @Override
                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                            RoutineAuth.TOKEN_FIREBASE = task.getResult().getToken();

                            useCaseCallback.onSuccess(new ExecLogin.ResponseValue());
                        }
                    });
                }
            }
        });
    }

    public void execLogout(UseCase.UseCaseCallback useCaseCallback) {
        firebaseAuth.signOut();

        if (!googleApiClient.isConnected()) {
            googleApiClient.blockingConnect();
        }

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.d("MY", "Sign out");
            }
        });

        useCaseCallback.onSuccess(new ExecLogout.ResponseValue());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");
    }
}
