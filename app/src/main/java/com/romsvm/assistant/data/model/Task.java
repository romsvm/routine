package com.romsvm.assistant.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Strings;

import java.util.UUID;

/**
 * Created by Roman on 27.02.2017.
 */

public class Task implements Parcelable {

    public enum Priority {
        EXTRA(0), HIGH(1), MEDIUM(2), LOW(3);

        private int value;

        Priority(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static class Builder {

        private String id;
        private String title;
        private String projectId;
        private boolean isComplete;
        private long dueDate;
        private Priority priority;
        private String comment;
        private long reminder;
        private long dateCreate;
        private long dateUpdate;

        public Builder(String title) {
            this.title = title;
            this.projectId = "";
            this.id = UUID.randomUUID().toString();
            this.comment = "";
            this.isComplete = false;
            this.priority = Priority.MEDIUM;
            this.dueDate = 0;
            this.reminder = 0;
            this.dateCreate = 0;
            this.dateUpdate = 0;
        }

        public Builder setId(String value) {
            this.id = value;
            return this;
        }

        public Builder setTitle(String value) {
            this.title = value;
            return this;
        }

        public Builder setProjectId(String value) {
            this.projectId = value;
            return this;
        }

        public Builder setIsComplete(boolean value) {
            this.isComplete = value;
            return this;
        }

        public Builder setDueDate(long value) {
            this.dueDate = value;
            return this;
        }

        public Builder setPriority(Priority value) {
            this.priority = value;
            return this;
        }

        public Builder setComment(String value) {
            this.comment = value;
            return this;
        }

        public Builder setReminder(long value) {
            this.reminder = value;
            return this;
        }

        public Builder setDateCreate(long value) {
            this.dateCreate = value;
            return this;
        }

        public Builder setDateUpdate(long value) {
            this.dateUpdate = value;
            return this;
        }

        public Task build() {
            return new Task(this);
        }
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel parcel) {
            return new Task(parcel);
        }

        @Override
        public Task[] newArray(int i) {
            return new Task[i];
        }
    };

    @NonNull
    private String id;
    @Nullable
    private String title;
    @NonNull
    private String projectId;
    private boolean isComplete;
    private long dueDate;
    private Priority priority;
    @Nullable
    private String comment;
    private long reminder;
    private long dateCreate;
    private long dateUpdate;

    public Task(Task.Builder builder) {
        this.id = builder.id;
        this.projectId = builder.projectId;
        this.title = builder.title;
        this.comment = builder.comment;
        this.isComplete = builder.isComplete;
        this.priority = builder.priority;
        this.dueDate = builder.dueDate;
        this.reminder = builder.reminder;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public Task(Parcel source) {
        this.id = source.readString();
        this.projectId = source.readString();
        this.title = source.readString();
        this.comment = source.readString();
        this.isComplete = source.readInt() != 0;
        this.priority = Priority.valueOf(source.readString());
        this.dueDate = source.readLong();
        this.reminder = source.readLong();
        this.dateCreate = source.readLong();
        this.dateUpdate = source.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.projectId);
        parcel.writeString(this.title);
        parcel.writeString(this.comment);
        parcel.writeInt(this.isComplete ? 1 : 0);
        parcel.writeString(this.priority.name());
        parcel.writeLong(this.dueDate);
        parcel.writeLong(this.reminder);
        parcel.writeLong(this.dateCreate);
        parcel.writeLong(this.dateUpdate);
    }

    @NonNull
    public String getId() {
        return this.id;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @NonNull
    public String getProjectId() {
        return this.projectId;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(int dueDate) {
        this.dueDate = dueDate;
    }

    public boolean getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(boolean complete) {
        isComplete = complete;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Nullable
    public String getComment() {
        return comment;
    }

    public void setComment(@Nullable String comment) {
        this.comment = comment;
    }

    public long getReminder() {
        return reminder;
    }

    public void setReminder(int reminder) {
        this.reminder = reminder;
    }

    public long getDateCreate() {
        return this.dateCreate;
    }

    public void setDateCreate(long value) {
        this.dateCreate = value;
    }

    public long getDateUpdate() {
        return this.dateUpdate;
    }

    public void setDateUpdate(long value) {
        this.dateUpdate = value;
    }

    public boolean isEmpty() {
        return Strings.isNullOrEmpty(title);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return this.toString().equals(o.toString());
    }

    @Override
    public String toString() {
        return this.title + " id " + this.id + " projectId " + projectId + " isComplete " + isComplete + " priority " + priority + " due " + dueDate + " reminder " + reminder + " comment " + comment + " dateCreate " + dateCreate + " dateUpdate " + dateUpdate;
    }
}
