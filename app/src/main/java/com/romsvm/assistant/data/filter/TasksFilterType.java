package com.romsvm.assistant.data.filter;

/**
 * Created by Roman on 11.04.2017.
 */

public enum TasksFilterType {
    BY_DATE,
    BY_PROJECT
}
