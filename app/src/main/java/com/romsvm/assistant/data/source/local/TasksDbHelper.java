package com.romsvm.assistant.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.romsvm.assistant.data.source.TasksPersistenceContract.ProjectEntry;
import com.romsvm.assistant.data.source.TasksPersistenceContract.TaskEntry;

/**
 * Created by Roman on 16.02.2017.
 */

public class TasksDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Assistant.db";

    private static final String NOT_NULL = " NOT NULL";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_PROJECT_ENTRIES =
            "CREATE TABLE " + ProjectEntry.TABLE_NAME + " (" +
                    ProjectEntry._ID + INTEGER_TYPE + NOT_NULL + " PRIMARY KEY" + COMMA_SEP +
                    ProjectEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    ProjectEntry.COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    ProjectEntry.COLUMN_NAME_DATE_CREATE + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                    ProjectEntry.COLUMN_NAME_DATE_UPDATE + INTEGER_TYPE + NOT_NULL + " );";

    private static final String SQL_CREATE_TASK_ENTRIES =
            "CREATE TABLE " + TaskEntry.TABLE_NAME + " (" +
                    TaskEntry._ID + INTEGER_TYPE + NOT_NULL + " PRIMARY KEY" + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_PROJECT_ID + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_COMPLETE + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_DUE_DATE + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_PRIORITY + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_COMMENT + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_REMINDER + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_DATE_CREATE + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_DATE_UPDATE + INTEGER_TYPE + NOT_NULL + ");";

    public TasksDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PROJECT_ENTRIES);
        db.execSQL(SQL_CREATE_TASK_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}


