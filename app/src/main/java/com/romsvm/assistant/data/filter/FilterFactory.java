/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.romsvm.assistant.data.filter;

import java.util.HashMap;
import java.util.Map;

/**
 * Factory of {@link TaskFilter}s.
 */
public class FilterFactory {

    private static final Map<TasksFilterType, TaskFilter> mFilters = init();

    private static Map<TasksFilterType, TaskFilter> init() {
        Map<TasksFilterType, TaskFilter> result = new HashMap<>();
        result.put(TasksFilterType.BY_DATE, new FilterByDate());
        result.put(TasksFilterType.BY_PROJECT, new FilterByProject());

        return result;
    }

    public FilterFactory() {

    }

    public static TaskFilter create(TasksFilterType filterType) {
        return mFilters.get(filterType);
    }
}
