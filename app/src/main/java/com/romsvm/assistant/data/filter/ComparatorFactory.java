package com.romsvm.assistant.data.filter;

import com.romsvm.assistant.data.model.Task;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Roman on 11.04.2017.
 */

public class ComparatorFactory {
    private static final Map<SortType, Comparator> comparatorMap = init();

    private static Map<SortType, Comparator> init() {

        HashMap<SortType, Comparator> result = new HashMap<>();

        Comparator<Task> comparatorPriority = new Comparator<Task>() {
            @Override
            public int compare(Task a, Task b) {

                int result = 0;

                if (a.getIsComplete() == b.getIsComplete()) {
                    if (a.getPriority().getValue() > b.getPriority().getValue()) {
                        result = 1;
                    } else if (a.getPriority().getValue() < b.getPriority().getValue()) {
                        result = -1;
                    }
                } else if (!a.getIsComplete() && b.getIsComplete()) {
                    result = -1;
                } else if (a.getIsComplete() && !b.getIsComplete()) {
                    result = 1;
                }

                return result;
            }
        };

        Comparator<Task> comparatorCreationDate = new Comparator<Task>() {
            @Override
            public int compare(Task a, Task b) {

                int result = 0;

                if (a.getIsComplete() == b.getIsComplete()) {
                    if (a.getDateCreate() > b.getDateCreate()) {
                        result = 1;
                    } else if (a.getDateCreate() < b.getDateCreate()) {
                        result = -1;
                    }
                } else if (!a.getIsComplete() && b.getIsComplete()) {
                    result = -1;
                } else if (a.getIsComplete() && !b.getIsComplete()) {
                    result = 1;
                }

                return result;
            }
        };

        Comparator<Task> comparatorDueDate = new Comparator<Task>() {
            @Override
            public int compare(Task a, Task b) {

                int result = 0;

                if (a.getIsComplete() == b.getIsComplete()) {
                    if (a.getDueDate() > b.getDueDate()) {
                        result = 1;
                    } else if (a.getDueDate() < b.getDueDate()) {
                        result = -1;
                    }
                } else if (!a.getIsComplete() && b.getIsComplete()) {
                    result = -1;
                } else if (a.getIsComplete() && !b.getIsComplete()) {
                    result = 1;
                }

                return result;
            }
        };

        result.put(SortType.PRIORITY, comparatorPriority);
        result.put(SortType.CREATION_DATE, comparatorCreationDate);
        result.put(SortType.DUE_DATE, comparatorDueDate);

        return result;
    }

    public static Comparator<Task> create(SortType sortType) {
        return comparatorMap.get(sortType);
    }
}
