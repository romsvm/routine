package com.romsvm.assistant.data.source;

import android.provider.BaseColumns;

/**
 * Created by Roman on 16.02.2017.
 */

public final class TasksPersistenceContract {

    private TasksPersistenceContract() {

    }

    public static abstract class ProjectEntry implements BaseColumns {
        public static final String TABLE_NAME = "projects";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_ENTRY_ID = "id";
        public static final String COLUMN_NAME_DATE_CREATE = "dateCreate";
        public static final String COLUMN_NAME_DATE_UPDATE = "dateUpdate";
    }

    public static abstract class TaskEntry implements BaseColumns {
        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_NAME_PROJECT_ID = "projectId";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_ENTRY_ID = "id";
        public static final String COLUMN_NAME_COMPLETE = "isComplete";
        public static final String COLUMN_NAME_DUE_DATE = "dueDate";
        public static final String COLUMN_NAME_PRIORITY = "priority";
        public static final String COLUMN_NAME_COMMENT = "comment";
        public static final String COLUMN_NAME_REMINDER = "reminder";
        public static final String COLUMN_NAME_DATE_CREATE = "dateCreate";
        public static final String COLUMN_NAME_DATE_UPDATE = "dateUpdate";
    }
}