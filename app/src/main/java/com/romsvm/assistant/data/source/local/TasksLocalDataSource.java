package com.romsvm.assistant.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.romsvm.assistant.UseCase;
import com.romsvm.assistant.data.filter.TasksFilterType;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.data.source.TasksDataSource;
import com.romsvm.assistant.data.source.TasksPersistenceContract.TaskEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.romsvm.assistant.data.source.TasksPersistenceContract.ProjectEntry;

/**
 * Created by Roman on 24.02.2017.
 */

public class TasksLocalDataSource implements TasksDataSource {

    private static TasksLocalDataSource INSTANCE;

    public static TasksLocalDataSource getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new TasksLocalDataSource(context);
        }

        return INSTANCE;
    }

    private TasksLocalDataSource(@NonNull Context context) {
        checkNotNull(context);
        dbHelper = new TasksDbHelper(context);
    }

    private TasksDbHelper dbHelper;

    @Override
    public void synchronize(UseCase.UseCaseCallback callback) {

    }

    @Override
    public void getAllProjects(@NonNull GetAllProjectsCallback callback) {

        List<Project> projects = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursorProjects =
                db.query(ProjectEntry.TABLE_NAME, null, null, null, null, null, null);

        if (cursorProjects != null && cursorProjects.getCount() > 0) {

            projects = new ArrayList<>();

            while (cursorProjects.moveToNext()) {
                String projectId = cursorProjects.getString(
                        cursorProjects.getColumnIndexOrThrow(ProjectEntry.COLUMN_NAME_ENTRY_ID));
                String projectTitle = cursorProjects.getString(
                        cursorProjects.getColumnIndexOrThrow(ProjectEntry.COLUMN_NAME_TITLE));
                long dateCreate = cursorProjects.getInt(cursorProjects.getColumnIndexOrThrow(
                        ProjectEntry.COLUMN_NAME_DATE_CREATE));
                long dateUpdate = cursorProjects.getInt(cursorProjects.getColumnIndexOrThrow(
                        ProjectEntry.COLUMN_NAME_DATE_UPDATE));

                Project project = new Project(projectId, projectTitle, dateCreate, dateUpdate);
                projects.add(project);
            }

            Collections.sort(projects, new Comparator<Project>() {
                @Override
                public int compare(Project a, Project b) {

                    int result = 0;

                    if (a.getDateCreate() > b.getDateCreate()) {
                        result = 1;
                    } else if (a.getDateCreate() < b.getDateCreate()) {
                        result = -1;
                    }

                    return result;
                }
            });
        }

        if (cursorProjects != null) {
            cursorProjects.close();
        }

        db.close();

        if (projects == null) {
            callback.onDataNotAvailable();
        } else {
            callback.onProjectsLoaded(projects);
        }
    }

    @Override
    public void getProject(@NonNull String projectId, @NonNull GetProjectCallback callback) {

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = ProjectEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {projectId};

        Cursor c = db.query(
                ProjectEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        Project project = null;

        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            String itemId = c.getString(c.getColumnIndexOrThrow(ProjectEntry.COLUMN_NAME_ENTRY_ID));
            String projectTitle =
                    c.getString(c.getColumnIndexOrThrow(ProjectEntry.COLUMN_NAME_TITLE));
            long dateCreate =
                    c.getLong(c.getColumnIndexOrThrow(ProjectEntry.COLUMN_NAME_DATE_CREATE));
            long dateUpdate =
                    c.getLong(c.getColumnIndexOrThrow(ProjectEntry.COLUMN_NAME_DATE_UPDATE));

            project = new Project(itemId, projectTitle, dateCreate, dateUpdate);
        }
        if (c != null) {
            c.close();
        }

        db.close();

        if (project != null) {
            callback.onProjectLoaded(project);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void deleteAllProjects() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(ProjectEntry.TABLE_NAME, null, null);
        db.close();
    }

    @Override
    public void deleteProject(@NonNull String projectId) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String[] selectionArgs = {projectId};

        String selectionTask = TaskEntry.COLUMN_NAME_PROJECT_ID + " LIKE ?";

        db.delete(TaskEntry.TABLE_NAME, selectionTask, selectionArgs);

        String selectionProject = ProjectEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";

        db.delete(ProjectEntry.TABLE_NAME, selectionProject, selectionArgs);

        db.close();
    }

    @Override
    public void deleteTask(@NonNull String taskId) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String[] selectionArgs = {taskId};

        String selectionTask = TaskEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";

        db.delete(TaskEntry.TABLE_NAME, selectionTask, selectionArgs);

        db.close();
    }

    @Override
    public void refresh() {
        // Not required because the {@link ProjectRepository} handles the logic of refreshing the
        // tasks from all the available data sources.
    }

    @Override
    public void saveProject(@NonNull Project project) {
        checkNotNull(project);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues valuesProject = new ContentValues();
        valuesProject.put(ProjectEntry.COLUMN_NAME_ENTRY_ID, project.getId());
        valuesProject.put(ProjectEntry.COLUMN_NAME_TITLE, project.getTitle());
        valuesProject.put(ProjectEntry.COLUMN_NAME_DATE_CREATE, project.getDateCreate());
        valuesProject.put(ProjectEntry.COLUMN_NAME_DATE_UPDATE, project.getDateUpdate());

        db.insertOrThrow(ProjectEntry.TABLE_NAME, null, valuesProject);

        db.close();
    }

    @Override
    public void updateProject(@NonNull Project project) {
        checkNotNull(project);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues valuesProject = new ContentValues();
        valuesProject.put(ProjectEntry.COLUMN_NAME_TITLE, project.getTitle());
        valuesProject.put(ProjectEntry.COLUMN_NAME_DATE_UPDATE, project.getDateUpdate());

        String selection = ProjectEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {project.getId()};

        db.update(ProjectEntry.TABLE_NAME, valuesProject, selection, selectionArgs);
        db.close();
    }

    @Override
    public void getAllTasks(@NonNull GetAllTasksCallback callback) {
        checkNotNull(callback);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = null;
        String[] selectionArgs = null;

        Cursor cursorTasks =
                db.query(TaskEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        ArrayList<Task> list = null;

        if (cursorTasks != null && cursorTasks.getCount() > 0) {

            list = new ArrayList<>(0);

            while (cursorTasks.moveToNext()) {
                list.add(createTask(cursorTasks));
            }

            Collections.sort(list, new Comparator<Task>() {
                @Override
                public int compare(Task a, Task b) {

                    int result = 0;

                    if (a.getDateCreate() > b.getDateCreate()) {
                        result = 1;
                    } else if (a.getDateCreate() < b.getDateCreate()) {
                        result = -1;
                    }

                    return result;
                }
            });
        }

        db.close();

        if (list != null) {
            callback.onTasksLoaded(list);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void getTask(@NonNull final String taskId, @NonNull final GetTaskCallback callback) {
        checkNotNull(taskId);
        checkNotNull(callback);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = TaskEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {taskId};

        Cursor cursorTasks = db.query(
                TaskEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        Task task = null;

        if (cursorTasks != null && cursorTasks.getCount() > 0) {
            cursorTasks.moveToFirst();
            task = createTask(cursorTasks);
        }
        if (cursorTasks != null) {
            cursorTasks.close();
        }

        db.close();

        if (task != null) {
            callback.onTaskLoaded(task);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void getTasksByFilter(String filterName, String filterValue,
                                 @NonNull GetTasksByFilterCallback callback) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection;
        String[] selectionArgs;

        if (filterName.equals(TasksFilterType.BY_DATE.name())) {
            selection = TaskEntry.COLUMN_NAME_DUE_DATE + " LIKE ?";
            selectionArgs = new String[]{filterValue};
        } else if (filterName.equals(TasksFilterType.BY_PROJECT.name())) {
            selection = TaskEntry.COLUMN_NAME_PROJECT_ID + " LIKE ?";
            selectionArgs = new String[]{filterValue};
        } else {
            selection = null;
            selectionArgs = null;
        }

        Cursor cursorTasks =
                db.query(TaskEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        ArrayList<Task> list = new ArrayList<>(0);

        if (cursorTasks != null && cursorTasks.getCount() > 0) {
            while (cursorTasks.moveToNext()) {
                list.add(createTask(cursorTasks));
            }
        }

        db.close();

        if (list != null) {
            callback.onTasksLoaded(list);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void saveTask(@NonNull final Task task) {
        checkNotNull(task);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.insertOrThrow(TaskEntry.TABLE_NAME, null, createContentValuesTask(task));

        db.close();
    }

    @Override
    public void updateSelectedTasks(@NonNull final List<Task> taskList) {
        checkNotNull(taskList);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selectionTask = TaskEntry.COLUMN_NAME_ENTRY_ID + " = ?";

        for (Task task : taskList) {

            String[] selectionArgs = {task.getId()};

            db.update(TaskEntry.TABLE_NAME, createContentValuesTask(task), selectionTask,
                      selectionArgs);
        }

        db.close();
    }

    @Override
    public void deleteAllTasks() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(TaskEntry.TABLE_NAME, null, null);
        db.close();
    }

    @Override
    public void deleteAll() {
        deleteAllProjects();
        deleteAllTasks();
    }

    private Task createTask(Cursor cursorTask) {

        String taskId = cursorTask
                .getString(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_ENTRY_ID));
        String projectId = cursorTask
                .getString(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_PROJECT_ID));
        String title =
                cursorTask.getString(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_TITLE));
        boolean isComplete = cursorTask
                .getInt(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_COMPLETE)) == 1;
        long dueDate = cursorTask
                .getLong(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_DUE_DATE));
        Task.Priority priority = Enum.valueOf(Task.Priority.class, cursorTask
                .getString(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_PRIORITY)));
        String comment = cursorTask
                .getString(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_COMMENT));
        long reminder = cursorTask
                .getLong(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_REMINDER));
        long dateCreate = cursorTask
                .getLong(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_DATE_CREATE));
        long dateUpdate = cursorTask
                .getLong(cursorTask.getColumnIndexOrThrow(TaskEntry.COLUMN_NAME_DATE_UPDATE));

        return new Task.Builder(title)
                .setId(taskId)
                .setProjectId(projectId)
                .setComment(comment)
                .setIsComplete(isComplete)
                .setDueDate(dueDate)
                .setPriority(priority)
                .setReminder(reminder)
                .setDateCreate(dateCreate)
                .setDateUpdate(dateUpdate)
                .build();
    }

    private ContentValues createContentValuesTask(Task task) {
        ContentValues result = new ContentValues();
        result.put(TaskEntry.COLUMN_NAME_ENTRY_ID, task.getId());
        result.put(TaskEntry.COLUMN_NAME_TITLE, task.getTitle());
        result.put(TaskEntry.COLUMN_NAME_PROJECT_ID, task.getProjectId());
        result.put(TaskEntry.COLUMN_NAME_COMPLETE, task.getIsComplete());
        result.put(TaskEntry.COLUMN_NAME_DUE_DATE, task.getDueDate());
        result.put(TaskEntry.COLUMN_NAME_PRIORITY, task.getPriority().name());
        result.put(TaskEntry.COLUMN_NAME_COMMENT, task.getComment());
        result.put(TaskEntry.COLUMN_NAME_REMINDER, task.getReminder());
        result.put(TaskEntry.COLUMN_NAME_DATE_CREATE, task.getDateCreate());
        result.put(TaskEntry.COLUMN_NAME_DATE_UPDATE, task.getDateUpdate());
        return result;
    }
}


