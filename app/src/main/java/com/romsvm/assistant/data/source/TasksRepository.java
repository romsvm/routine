package com.romsvm.assistant.data.source;

import android.support.annotation.NonNull;

import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;
import com.romsvm.assistant.launch.domen.usecase.Sync;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 24.02.2017.
 */

public class TasksRepository implements TasksDataSource {

    private static final String TAG = "TaskRepository";
    private static TasksRepository INSTANCE = null;

    public static TasksRepository getInstance(TasksDataSource tasksLocalDataSource,
                                              TasksDataSource tasksRemoteDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new TasksRepository(tasksLocalDataSource, tasksRemoteDataSource);
        }

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    boolean cacheProjectsIsDirty = true;
    boolean cacheTasksIsDirty = true;
    Map<String, Task> cachedTasks;
    Map<String, Project> cachedProjects;
    private final TasksDataSource tasksLocalDataSource;
    private final TasksDataSource tasksRemoteDataSource;

    private TasksRepository(@NonNull TasksDataSource tasksLocalDataSource,
                            @NonNull TasksDataSource tasksRemoteDataSource) {
        this.tasksLocalDataSource = checkNotNull(tasksLocalDataSource);
        this.tasksRemoteDataSource = checkNotNull(tasksRemoteDataSource);
        cachedProjects = new LinkedHashMap<>();
        cachedTasks = new LinkedHashMap<>();
    }

    @Override
    public void synchronize(@NonNull final Sync.UseCaseCallback callback) {

        checkNotNull(callback);

        final ArrayList<Project> remoteProjects = new ArrayList<>();
        final ArrayList<Project> localProjects = new ArrayList<>();
        final ArrayList<Task> remoteTask = new ArrayList<>();
        final ArrayList<Task> localTask = new ArrayList<>();

        refresh();

        getProjectsFromRemoteDataSource(new GetAllProjectsCallback() {
            @Override
            public void onProjectsLoaded(List<Project> projects) {
                remoteProjects.addAll(projects);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onError();
            }
        });

        getTasksFromRemoteDataSource(new GetAllTasksCallback() {
            @Override
            public void onTasksLoaded(List<Task> tasks) {
                remoteTask.addAll(tasks);
            }

            @Override
            public void onDataNotAvailable() {

                callback.onError();
            }
        });

        getAllProjects(new GetAllProjectsCallback() {
            @Override
            public void onProjectsLoaded(List<Project> projects) {

                localProjects.addAll(projects);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });

        getAllTasks(new GetAllTasksCallback() {
            @Override
            public void onTasksLoaded(List<Task> tasks) {

                localTask.addAll(tasks);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });

        long remoteTimestamp = getLastTimestamp(remoteProjects, remoteTask);
        long localTimestamp = getLastTimestamp(localProjects, localTask);

        //TODO как синхронизировать если несколько локальных источников
        if (remoteTimestamp > localTimestamp) {
        /*заходил с другого устройства и теперь захожу с этого
        * взять все с сервера*/
            refreshLocalProjects(remoteProjects);
            refreshLocalTasks(remoteTask);
        } else if (remoteTimestamp < localTimestamp) {

            //удаляю с удаленного источника все проекты, которые удалены с локального
            Iterator<Project> projectIterator = remoteProjects.iterator();

            while (projectIterator.hasNext()) {
                Project project = projectIterator.next();

                if (isProjectInList(project, localProjects) == null) {
                    tasksRemoteDataSource.deleteProject(project.getId());
                    projectIterator.remove();
                }
            }

            Iterator<Task> taskIterator = remoteTask.iterator();

            while (taskIterator.hasNext()) {
                Task task = taskIterator.next();
                if (isTaskInList(task, localTask) == null) {
                    tasksRemoteDataSource.deleteTask(task.getId());
                    taskIterator.remove();
                }
            }

            //Сохранить проекты и задачи, которые отсутствуют или отличаются
            for (Project pLocal : localProjects) {

                Project pRemote = isProjectInList(pLocal, remoteProjects);

                if (pRemote == null || (pRemote != null && !pRemote.equals(pLocal))) {
                    tasksRemoteDataSource.saveProject(pLocal);
                }
            }

            for (Task tLocal : localTask) {

                Task tRemote = isTaskInList(tLocal, remoteTask);

                if (tRemote == null || (tRemote != null && !tRemote.equals(tLocal))) {
                    tasksRemoteDataSource.saveTask(tLocal);
                }
            }

        }

        callback.onSuccess(new Sync.ResponseValue());
    }

    private Project isProjectInList(Project project, ArrayList<Project> list) {
        for (Project p : list) {
            if (p.getId().equals(project.getId())) {
                return p;
            }
        }

        return null;
    }

    private Task isTaskInList(Task task, ArrayList<Task> list) {
        for (Task t : list) {
            if (t.getId().equals(task.getId())) {
                return t;
            }
        }

        return null;
    }

    private long getLastTimestamp(List<Project> projects, List<Task> tasks) {
        long result = 0;

        for (Project project : projects) {
            if (project.getDateUpdate() > result) {
                result = project.getDateUpdate();
            }
        }

        for (Task task : tasks) {
            if (task.getDateUpdate() > result) {
                result = task.getDateUpdate();
            }
        }

        return result;
    }

    @Override
    public synchronized void getAllProjects(@NonNull final GetAllProjectsCallback callback) {
        checkNotNull(callback);

        if (!cacheProjectsIsDirty) {
            callback.onProjectsLoaded(new ArrayList<Project>(cachedProjects.values()));
        } else {
            tasksLocalDataSource.getAllProjects(new GetAllProjectsCallback() {
                @Override
                public void onProjectsLoaded(List<Project> projects) {
                    refreshCachedProjects(projects);
                    callback.onProjectsLoaded(projects);
                }

                @Override
                public void onDataNotAvailable() {
                    //TODO Обработать
                }
            });
        }
    }

    private void getProjectsFromRemoteDataSource(@NonNull final GetAllProjectsCallback callback) {
        tasksRemoteDataSource.getAllProjects(new GetAllProjectsCallback() {
            @Override
            public void onProjectsLoaded(List<Project> projects) {
                callback.onProjectsLoaded(projects);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    private void refreshCachedProjects(List<Project> projectList) {
        cachedProjects.clear();
        for (Project project : projectList) {
            cachedProjects.put(project.getId(), project);
        }
        cacheProjectsIsDirty = false;
    }

    private void refreshLocalProjects(List<Project> projectList) {
        tasksLocalDataSource.deleteAllProjects();
        for (Project project : projectList) {
            tasksLocalDataSource.saveProject(project);
        }
    }

    public Project getProjectWithId(@NonNull String projectId) {
        checkNotNull(projectId);
        if (cachedProjects.isEmpty()) {
            return null;
        } else {
            return cachedProjects.get(projectId);
        }
    }

    @Override
    public synchronized void getProject(@NonNull final String projectId,
                                        @NonNull final GetProjectCallback callback) {
        checkNotNull(projectId);
        checkNotNull(callback);

        Project cachedProject = getProjectWithId(projectId);

        if (cachedProject != null) {
            callback.onProjectLoaded(cachedProject);
            return;
        }

        tasksLocalDataSource.getProject(projectId, new GetProjectCallback() {
            @Override
            public void onProjectLoaded(Project project) {
                callback.onProjectLoaded(project);
            }

            @Override
            public void onDataNotAvailable() {
                tasksRemoteDataSource.getProject(projectId, new GetProjectCallback() {
                    @Override
                    public void onProjectLoaded(Project project) {
                        callback.onProjectLoaded(project);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }
                });
            }
        });
    }

    @Override
    public synchronized void saveProject(@NonNull Project project) {
        checkNotNull(project);
        tasksLocalDataSource.saveProject(project);
        //tasksRemoteDataSource.saveProject(project);
        cachedProjects.put(project.getId(), project);
    }

    @Override
    public synchronized void updateProject(@NonNull Project project) {
        checkNotNull(project);
        tasksLocalDataSource.updateProject(project);
        //tasksRemoteDataSource.updateProject(project);
        cachedProjects.put(project.getId(), project);
    }

    @Override
    public synchronized void deleteProject(@NonNull String projectId) {
        tasksLocalDataSource.deleteProject(projectId);
        //tasksRemoteDataSource.deleteProject(projectId);

        cachedProjects.remove(projectId);

        Iterator<String> iterator = cachedTasks.keySet().iterator();

        while (iterator.hasNext()) {
            if (cachedTasks.get(iterator.next()).getProjectId().equals(projectId)) {
                iterator.remove();
            }
        }
    }

    @Override
    public synchronized void deleteAllProjects() {
        tasksLocalDataSource.deleteAllProjects();
        //tasksRemoteDataSource.deleteAllProjects();
        cachedProjects.clear();
    }

    @Override
    public synchronized void refresh() {
        cacheProjectsIsDirty = true;
        cacheTasksIsDirty = true;
    }

    @Override
    public synchronized void getAllTasks(@NonNull final GetAllTasksCallback callback) {
        checkNotNull(callback);

        if (!cacheTasksIsDirty) {
            callback.onTasksLoaded(new ArrayList<>(cachedTasks.values()));
        } else {
            tasksLocalDataSource.getAllTasks(new GetAllTasksCallback() {
                @Override
                public void onTasksLoaded(List<Task> tasks) {
                    refreshCachedTasks(tasks);
                    callback.onTasksLoaded(new ArrayList<>(cachedTasks.values()));
                }

                @Override
                public void onDataNotAvailable() {
                    //TODO Обработать
                }
            });
        }
    }

    private void getTasksFromRemoteDataSource(final GetAllTasksCallback callback) {
        tasksRemoteDataSource.getAllTasks(new GetAllTasksCallback() {
            @Override
            public void onTasksLoaded(List<Task> tasks) {
                callback.onTasksLoaded(tasks);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    private void refreshCachedTasks(List<Task> tasks) {
        cachedTasks.clear();
        for (Task task : tasks) {
            cachedTasks.put(task.getId(), task);
        }

        cacheTasksIsDirty = false;
    }

    private void refreshLocalTasks(List<Task> tasks) {
        tasksLocalDataSource.deleteAllTasks();
        for (Task task : tasks) {
            tasksLocalDataSource.saveTask(task);
        }
    }

    @Override
    public synchronized void getTask(@NonNull final String taskId,
                                     @NonNull final GetTaskCallback callback) {
        checkNotNull(taskId);
        checkNotNull(callback);

        Task cachedTask = getTaskWithId(taskId);

        if (cachedTask != null) {
            callback.onTaskLoaded(cachedTask);
            return;
        }

        tasksLocalDataSource.getTask(taskId, new GetTaskCallback() {
            @Override
            public void onTaskLoaded(Task task) {
                callback.onTaskLoaded(task);
            }

            @Override
            public void onDataNotAvailable() {
                tasksRemoteDataSource.getTask(taskId, new GetTaskCallback() {
                    @Override
                    public void onTaskLoaded(Task task) {
                        callback.onTaskLoaded(task);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }
                });
            }
        });
    }

    private Task getTaskWithId(@NonNull String taskId) {
        checkNotNull(taskId);
        return cachedTasks.get(taskId);
    }

    @Override
    public synchronized void getTasksByFilter(String filterName, String filterValue,
                                              @NonNull final GetTasksByFilterCallback callback) {
        checkNotNull(callback);

        tasksLocalDataSource
                .getTasksByFilter(filterName, filterValue, new GetTasksByFilterCallback() {
                    @Override
                    public void onTasksLoaded(List<Task> taskList) {
                        callback.onTasksLoaded(taskList);
                    }

                    @Override
                    public void onDataNotAvailable() {

                    }
                });
    }

    @Override
    public synchronized void saveTask(@NonNull final Task task) {
        checkNotNull(task);

        tasksLocalDataSource.saveTask(task);
        //tasksRemoteDataSource.saveTask(task);
        cachedTasks.put(task.getId(), task);
    }

    @Override
    public synchronized void updateSelectedTasks(@NonNull final List<Task> taskList) {
        checkNotNull(taskList);
        tasksLocalDataSource.updateSelectedTasks(taskList);
        //tasksRemoteDataSource.updateSelectedTasks(taskList);

        for (Task task : taskList) {
            cachedTasks.put(task.getId(), task);
        }
    }

    @Override
    public synchronized void deleteTask(@NonNull String taskId) {
        tasksLocalDataSource.deleteTask(taskId);
        //tasksRemoteDataSource.deleteTask(taskId);
        cachedTasks.remove(taskId);
    }

    @Override
    public synchronized void deleteAllTasks() {
        tasksLocalDataSource.deleteAllTasks();
        //tasksRemoteDataSource.deleteAllTasks();
        cachedTasks.clear();
    }

    @Override
    public synchronized void deleteAll() {
        tasksLocalDataSource.deleteAll();
        //tasksRemoteDataSource.deleteAll();
        cachedProjects.clear();
        cachedTasks.clear();
    }
}