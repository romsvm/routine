package com.romsvm.assistant;

import android.content.Context;
import android.support.annotation.NonNull;

import com.romsvm.assistant.alarm.AppAlarmManager;
import com.romsvm.assistant.data.auth.RoutineAuth;
import com.romsvm.assistant.data.source.TasksRepository;
import com.romsvm.assistant.data.source.local.TasksLocalDataSource;
import com.romsvm.assistant.data.source.remote.TasksRemoteDataSource;
import com.romsvm.assistant.launch.domen.usecase.CheckUser;
import com.romsvm.assistant.launch.domen.usecase.ExecLogin;
import com.romsvm.assistant.launch.domen.usecase.Sync;
import com.romsvm.assistant.projectaddedit.domain.usecase.DeleteProject;
import com.romsvm.assistant.projectaddedit.domain.usecase.GetProject;
import com.romsvm.assistant.projectaddedit.domain.usecase.SaveProject;
import com.romsvm.assistant.projectaddedit.domain.usecase.UpdateProject;
import com.romsvm.assistant.selectedtasks.domain.usecase.ExecLogout;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetAllProjects;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetAllTasks;
import com.romsvm.assistant.selectedtasks.domain.usecase.GetTasksByFilter;
import com.romsvm.assistant.taskaddedit.domain.usecase.DeleteTask;
import com.romsvm.assistant.taskaddedit.domain.usecase.GetTask;
import com.romsvm.assistant.taskaddedit.domain.usecase.SaveTask;
import com.romsvm.assistant.taskaddedit.domain.usecase.UpdateTask;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roman on 24.02.2017.
 */

public class Injection {
    public static TasksRepository provideRepository(@NonNull Context context) {
        checkNotNull(context);
        return TasksRepository.getInstance(TasksLocalDataSource.getInstance(context),
                                           TasksRemoteDataSource.getInstance());
    }

    public static AppAlarmManager provideAppAlarmManager(@NonNull Context context) {
        checkNotNull(context);
        return AppAlarmManager.getInstance(context, provideRepository(context));
    }

    public static ExecLogin provideExecAuthorization(@NonNull Context context) {
        return new ExecLogin(RoutineAuth.getInstance(context));
    }

    public static ExecLogout provideExecLogout(@NonNull Context context) {
        return new ExecLogout(RoutineAuth.getInstance(context));
    }

    public static CheckUser provideCheckUser(@NonNull Context context) {
        return new CheckUser(RoutineAuth.getInstance(context));
    }

    public static Sync provideSync(@NonNull Context context) {
        return new Sync(provideRepository(context));
    }

    public static GetAllProjects provideGetAllProjects(@NonNull Context context) {
        return new GetAllProjects(provideRepository(context));
    }

    public static GetAllTasks provideGetAllTasks(@NonNull Context context) {
        return new GetAllTasks(provideRepository(context));
    }

    public static DeleteProject provideDeleteSelectedProjects(@NonNull Context context) {
        return new DeleteProject(provideRepository(context));
    }

    public static DeleteTask provideDeleteTask(@NonNull Context context) {
        return new DeleteTask(provideRepository(context));
    }

    public static GetTasksByFilter provideGetTasksByFilter(@NonNull Context context) {
        return new GetTasksByFilter(provideRepository(context));
    }

    public static GetProject provideGetProject(@NonNull Context context) {
        return new GetProject(provideRepository(context));
    }

    public static SaveProject provideSaveProject(@NonNull Context context) {
        return new SaveProject(provideRepository(context));
    }

    public static UpdateProject provideUpdateProject(@NonNull Context context) {
        return new UpdateProject(provideRepository(context));
    }

    public static GetTask provideGetTask(@NonNull Context context) {
        return new GetTask(provideRepository(context));
    }

    public static SaveTask provideSaveTask(@NonNull Context context) {
        return new SaveTask(provideRepository(context), provideAppAlarmManager(context));
    }

    public static UpdateTask provideUpdateTask(@NonNull Context context) {
        return new UpdateTask(provideRepository(context), provideAppAlarmManager(context));
    }

    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }
}


