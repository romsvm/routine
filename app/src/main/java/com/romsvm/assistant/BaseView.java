package com.romsvm.assistant;

/**
 * Created by Roman on 23.02.2017.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}