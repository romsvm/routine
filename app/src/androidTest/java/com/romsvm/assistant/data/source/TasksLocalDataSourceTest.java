package com.romsvm.assistant.data.source;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.romsvm.assistant.data.source.local.TasksLocalDataSource;
import com.romsvm.assistant.data.model.Project;
import com.romsvm.assistant.data.model.Task;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by Roman on 03.03.2017.
 */

@RunWith(AndroidJUnit4.class)
public class TasksLocalDataSourceTest {

    private static final String TITLE1 = "title1";
    private static final String TITLE2 = "title2";
    private static final String TITLE3 = "title3";

    private TasksLocalDataSource tasksLocalDataSource;

    @Before
    public void setup() {
        tasksLocalDataSource = TasksLocalDataSource.getInstance(InstrumentationRegistry.getTargetContext());
    }

    @After
    public void cleanUp() {
        tasksLocalDataSource.deleteAllProjects();
    }

    @Test
    public void testPreconditions() {
        assertNotNull(tasksLocalDataSource);
    }

    @Test
    public void saveProject_retrievesProject() {
        final Project newProject = new Project(TITLE1);

        tasksLocalDataSource.saveProject(newProject);

        tasksLocalDataSource.getProject(newProject.getId(), new TasksDataSource.GetProjectCallback() {
            @Override
            public void onProjectLoaded(Project project) {
                assertThat(project, is(newProject));
            }

            @Override
            public void onDataNotAvailable() {
                fail("Callback error");
            }
        });
    }

    @Test
    public void deleteProject_noRetrievedProject() {
        final Project newProject1 = new Project(TITLE1);
        tasksLocalDataSource.saveProject(newProject1);

        tasksLocalDataSource.deleteProject(newProject1.getId());

        TasksDataSource.GetProjectCallback callback = mock(TasksDataSource.GetProjectCallback.class);
        tasksLocalDataSource.getProject(newProject1.getId(), callback);

        verify(callback).onDataNotAvailable();
        verify(callback, never()).onProjectLoaded(any(Project.class));
    }

    @Test
    public void deleteAllProject_emptyListOfRetrievedProject() {
        Project newProject = new Project(TITLE1);
        tasksLocalDataSource.saveProject(newProject);
        TasksDataSource.GetAllProjectsCallback callback = mock(TasksDataSource.GetAllProjectsCallback.class);

        tasksLocalDataSource.deleteAllProjects();

        tasksLocalDataSource.getAllProjects(callback);

        verify(callback).onDataNotAvailable();
        verify(callback, never()).onProjectsLoaded(anyList());
    }

    @Test
    public void updateProject_newProjectTitle() {
        final Project oldProject = new Project(TITLE1);
        tasksLocalDataSource.saveProject(oldProject);

        final Project newProject = new Project(oldProject.getId(), TITLE2, oldProject.getDateCreate(), new Date().getTime());
        tasksLocalDataSource.updateProject(newProject);

        TasksLocalDataSource.GetProjectCallback callback = new TasksDataSource.GetProjectCallback() {
            @Override
            public void onProjectLoaded(Project project) {
                assertThat(oldProject.getId(), equalTo(project.getId()));
                assertThat(project.getTitle(), equalTo(newProject.getTitle()));
            }

            @Override
            public void onDataNotAvailable() {
                fail("Callback error");
            }
        };

        tasksLocalDataSource.getProject(oldProject.getId(), callback);
    }

    @Test
    public void saveTask_retrievesTask() {
        Project newProject = new Project(TITLE1);
        tasksLocalDataSource.saveProject(newProject);

        final Task newTask = new Task.Builder(TITLE2).setProjectId(newProject.getId()).build();
        tasksLocalDataSource.saveTask(newTask);

        TasksDataSource.GetTaskCallback callback = new TasksDataSource.GetTaskCallback() {
            @Override
            public void onTaskLoaded(Task task) {
                assertThat(newTask, is(task));
            }

            @Override
            public void onDataNotAvailable() {
                fail("Callback error");
            }
        };

        tasksLocalDataSource.getTask(newTask.getId(), callback);
    }

    @Test
    public void deleteTask_noRetrievedTask() {

        Project newProject = new Project(TITLE1);
        tasksLocalDataSource.saveProject(newProject);

        final Task newTask1 = new Task.Builder(TITLE1).setProjectId(newProject.getId()).build();

        tasksLocalDataSource.saveTask(newTask1);

        tasksLocalDataSource.deleteTask(newTask1.getId());

        TasksDataSource.GetTaskCallback callback = mock(TasksDataSource.GetTaskCallback.class);
        tasksLocalDataSource.getTask(newTask1.getId(), callback);

        verify(callback).onDataNotAvailable();
        verify(callback, never()).onTaskLoaded(any(Task.class));
    }

    @Test
    public void updateTask_retrievesNewTitle() {
        Project newProject = new Project(TITLE1);
        tasksLocalDataSource.saveProject(newProject);

        Task oldTask = new Task.Builder(TITLE2).setProjectId(newProject.getId()).build();
        tasksLocalDataSource.saveTask(oldTask);

        final Task updatedTask = new Task.Builder(TITLE3)
                .setId(oldTask.getId())
                .setProjectId(oldTask.getProjectId())
                .setIsComplete(oldTask.getIsComplete())
                .setDueDate(oldTask.getDueDate())
                .setPriority(oldTask.getPriority())
                .setComment(oldTask.getComment())
                .setReminder(oldTask.getReminder())
                .setDateCreate(new Date().getTime())
                .build();

        tasksLocalDataSource.updateSelectedTasks(new ArrayList<Task>(Arrays.asList(updatedTask)));

        tasksLocalDataSource.getTask(oldTask.getId(), new TasksDataSource.GetTaskCallback() {
            @Override
            public void onTaskLoaded(Task task) {
                assertThat(task, is(updatedTask));
            }

            @Override
            public void onDataNotAvailable() {
                fail("Callback error");
            }
        });
    }
}
